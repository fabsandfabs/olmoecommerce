<?php

namespace Olmo\Ecommerce\Providers;

use Olmo\Ecommerce\Foundation\OlmoEcommerce;
use Illuminate\Support\ServiceProvider as LaravelAppServiceProvider;

class OlmoEcommerceProvider extends LaravelAppServiceProvider
{

    public function boot(): void
    {

    }

    public function register(): void
    {
        parent::register();
        
        // Register Core Facade Classes, should not be registered in the $aliases property, since they are used
        // by the auto-loading scripts, before the $aliases property is executed.
        $this->app->alias(OlmoEcommerce::class, 'OlmoEcommerce');
    }

}
