<?php

namespace Olmo\Ecommerce\App\Helpers;

use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class HelpersPayment
{

    public static function baseURL() {
        return [
            'sandbox' => "https://api-m.sandbox.paypal.com",
            'production' => "https://api-m.paypal.com"
        ];
    }

    public static function baseURLEnviroment() {

        $url = '';

        if(env('PAYPAL_MODE') == 'sandbox') {
            $url = self::baseURL()['sandbox'];
        } else if(env('PAYPAL_MODE') == 'live'){
            $url = self::baseURL()['production'];
        }

        return $url;
    }    

    public static function createOrderPaypal($order, $currency, $lang)
    {
        $accessToken = self::generateAccessTokenPaypal();

        if(!$accessToken){
            return response(400);
        }
        
        $url = self::baseURLEnviroment() . '/v2/checkout/orders';

        $body = [
            "purchase_units" => [
                [
                    "amount" => [
                        "currency_code" => $currency,
                        "value" => number_format((float)$order['total_read_general'], 2, '.', ''),
                    ],
                ],
            ],            
            "intent" => "CAPTURE",
            "payment_source" => [
                "paypal" => [
                    "experience_context" => [
                        "payment_method_preference" => "IMMEDIATE_PAYMENT_REQUIRED",
                        "payment_method_selected" => "PAYPAL",
                        "brand_name" => env('PAYPAL_BRANDNAME'),
                        "locale" => $lang,
                        "landing_page" => "LOGIN",
                        // "shipping_preference" => $order['shippingname_read_shipping'].' '.$order['shippingsurname_read_shipping'].' '.$order['shippingsurname_read_shipping'],
                        "user_action" => "PAY_NOW",
                        "return_url" => env('FRONT_URL') . "/checkout-completed/?order=" . $order['code_read_general'],
                        "cancel_url" => env('FRONT_URL') . "/checkout-canceled/?order=" . $order['code_read_general']
                    ]
                ]
            ]         
        ];

        $body = json_encode($body, true);

        $response = Http::withHeaders([
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $accessToken['access_token'],
            'PayPal-Request-Id' => $order['code_read_general']
        ])->withBody($body, 'application/json')->post($url);

        if($response->status() == 200){
            $paypalOrder = $response->json();
            Db::table('olmo_order')->where('code_read_general', $order['code_read_general'])->update([
                'paypalid_txt_general' => $paypalOrder['id']
            ]);                        
        }

        return $response;

    }

    public static function checkoutOrderPaypalCreditCard($order, $lang, $expiry, $name, $number, $security)
    {

        $accessToken = self::generateAccessTokenPaypal();

        if(!$accessToken){
            return response(400);
        }        

        $url = self::baseURLEnviroment() . '/v2/checkout/orders/' . $order['paypalid'] . '/confirm-payment-source';

        $date = explode('/', $expiry);
        $dateTimeFormatExpiry = '20'.$date[1] . '-' . $date[0];

        $cardnumber = str_replace(' ', '', $number);

        $body = [
            "purchase_units" => [
                [
                    "amount" => [
                        "currency_code" => $order['currency'],
                        "value" => $order['total'],
                    ],
                ],
            ],            
            "intent" => "CAPTURE",
            "payment_source" => [
                "card" => [
                    // "billing_address" => [
                    //     "address_line_1" => $order['billingaddress_read_billing'],
                    //     "admin_area_1" => $order['billingstate_read_billing'],
                    //     "admin_area_2" => $order['billingcity_read_billing'],
                    //     "country_code" => $order['billingcountry_read_billing'],
                    //     "postal_code" => $order['billingZIP_read_billing']
                    // ],
                    "expiry" => $dateTimeFormatExpiry,
                    "name" => $name,
                    "number" => $cardnumber,
                    "security_code" => $security,
                ]
            ]         
        ];

        $body = json_encode($body, true);        

        $response = Http::withHeaders([
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $accessToken['access_token'],
            'PayPal-Request-Id' => $order['code']
        ])->withBody($body, 'application/json')->post($url);

        // $response = self::authorizePaymentPaypal($order['code']);

        return $response;

    }

    public static function authorizePaymentPaypal($code)
    {

        $accessToken = self::generateAccessTokenPaypal();

        if(!$accessToken){
            return response(400);
        }

        $url = self::baseURLEnviroment() . '/v2/checkout/orders/'.$code.'/authorize';

        $response = Http::withHeaders([
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $accessToken['access_token'],
            'PayPal-Request-Id' => $code
        ])->post($url);
        // $response = Http::withToken($accessToken['access_token'])->post($url);

        return $response;
    }

    public static function capturePaymentPaypal($orderId)
    {        

        $accessToken = self::generateAccessTokenPaypal();

        if(!$accessToken){
            return response(400);
        }
        
        $url = self::baseURLEnviroment() . '/v2/checkout/orders/'.$orderId.'/capture';

        $body = [
            "orderID" => $orderId            
        ];

        $body = json_encode($body, true); 

        $response = Http::withToken($accessToken['access_token'])->withBody($body, 'application/json')->post($url);
        
        return $response;
    }

    public static function generateClientTokenPaypal($lang, $customer)
    {
        $accessToken = self::generateAccessTokenPaypal();

        if(!$accessToken){
            return response(400);
        }   

        $url = self::baseURLEnviroment() . '/v1/identity/generate-token';

        $body = [
            "customer_id" => $customer['id']
        ];

        $response = Http::withHeaders([
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $accessToken['access_token'],
            'Accept-Language' => $lang,
        ])->post($url, $body);

        return $response;

    }

    public static function generateAccessTokenPaypal()
    {        
        $url = self::baseURLEnviroment() . '/v1/oauth2/token';

        $response = Http::withBasicAuth(env('CLIENT_ID'), env('APP_SECRET'))->asForm()->post($url, ["grant_type" => "client_credentials"]);

        return $response;
    }

    
}