<?php

namespace Olmo\Ecommerce\App\Helpers;

use Illuminate\Support\Facades\DB;
use Olmo\Forms\App\Http\Controller\FormController;


class HelpersEmail
{

    public static function sendEmail($customer, $data, $lang, $formtype, $a = true)
    {
        /**
         * Send Recap-email to the customer and the master
         */
        $paramsForm = [            
            'email'  => $customer['email'],
            'domain' => env('APP_URL'),
            'locale' => $lang
        ];

        $params = array_merge($data, $paramsForm);    
        
        if($a){
            $afield = [
                'a' => $customer['email'],
            ];
            $params = array_merge($params, $afield);
        }

        $forms = Db::table('olmo_form')->get();        

        foreach ($forms as $form) {
            $ids = explode(',', $form->formtype_multid_general);
            if (in_array($formtype, $ids)) {
                $formid = $form->id;
                $params['id'] = $formid;
            }
        }

        $send = FormController::sendingFormData($params, 'internal');

        return $send;
    } 
    
}