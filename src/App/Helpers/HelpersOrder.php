<?php

namespace Olmo\Ecommerce\App\Helpers;

use Illuminate\Support\Facades\DB;

class HelpersOrder
{

    /**
     * Reset cart and subtract quantity
     */    
    public static function resetCartSubtractQty($customer)
    {
        
        if(@$customer['xguest'] != ''){
            $userId = $customer['xguest'];
            $cartIds = Db::table('olmo_cart')->where('guest_id', $userId)->where('active', "1")->get();            
        } else {
            $userId = $customer['id'];
            $cartIds = Db::table('olmo_cart')->where('customer_id', $userId)->where('active', "1")->get();            
        }        

        foreach($cartIds as $id) {

            $items = Db::table('olmo_cartitem')->where('cart_id', $id->id)->get();

            foreach($items as $item) {
                if($item->properties){
                    $productitem = HelpersCart::getProductItem($item->item_id, $item->properties);
                    if(!env("CHECK_QUANTITY_CUSTOM", false)){
                        Db::table('olmo_quantity')->where('prod_id', $productitem[0]->id)->decrement('quantity', $item->item_qty);
                    }
                    Db::table('olmo_cartitem')->where('cart_id', $id->id)->delete();
                } else {
                    if(!env("CHECK_QUANTITY_CUSTOM", false)){
                        Db::table('olmo_quantity')->where('prod_id', $item->id)->where('type', 'product')->decrement('quantity', $item->item_qty);
                    }
                    Db::table('olmo_cartitem')->where('cart_id', $id->id)->delete();                    
                }

            }

        }
        if(@$customer['xguest'] != ''){
            $cart = Db::table('olmo_cart')->where('guest_id', $userId)->update(['active' => 0]);  
        } else {
            $cart = Db::table('olmo_cart')->where('customer_id', $userId)->update(['active' => 0]);  
        }                  

        return $cart;
    }
}