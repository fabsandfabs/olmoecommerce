<?php

namespace Olmo\Ecommerce\App\Helpers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\File;

use Olmo\Core\App\Helpers\HelpersCustomer;
use Olmo\Core\App\Helpers\HelpersLang;
use Olmo\Core\App\Helpers\HelpersSerializer;
use App\Containers\Frontend\Quantity\Controllers\Controller as Quantity;

class HelpersCart
{
    public static function getProductItem($id, $properties)
    {
        $defaultLang = HelpersLang::getDefaultLang();
        $props = json_decode($properties, true);
        $filter  = '';
        $regex   = "'";
        foreach($props as $value){
            $regex .= ','.$value.',|';
        }
        $regex = substr($regex, 0, -1)."'";
        $filter .= " CONCAT(',',property_props_general,',') REGEXP $regex ";

        $sql = "SELECT * FROM olmo_productitem WHERE ".$filter."AND enabled_is_general = 'true' AND locale_hidden_general = '$defaultLang' AND postid_hidden_general = '$id'";
        // return $sql;
        $variants = DB::select($sql);

        return $variants;
    }

    public static function getProductVersion($id, $properties, $lang)
    {
        $defaultLang = HelpersLang::getDefaultLang();

        if($defaultLang != $lang){
            $currentitem = DB::table('olmo_product')->where('id', $id)->first();
            $item = DB::table('olmo_product')->where('id', $currentitem->parentid_hidden_general)->first();
            if($item){
                $id = $item->id;
            }
        }

        if(gettype($properties) == 'string'){
            $props = explode(',', $properties);
        } else {
            $props = $properties;
        }

        $filter  = '';
        $regex   = "'";
        foreach($props as $value){
            $regex .= $value.'|';
        }
        $regex = substr($regex, 0, -1)."'";
        $filter .= " CONCAT(',',property_props_general,',') REGEXP $regex ";

        $sql = "SELECT * FROM olmo_productitem WHERE ".$filter."AND enabled_is_general = 'true' AND locale_hidden_general = '$defaultLang' AND postid_hidden_general = '$id'";

        $variants = DB::select($sql);

        if(count($variants) > 1){

            foreach($variants as $item){
                $propsProduct = explode(',', $item->property_props_general);
                $counting = count(array_intersect($props, $propsProduct));
                if(count($props) == $counting){
                    return $item->id;
                };
            }

        } else if(count($variants) > 0){
            return $variants[0];
        }

        return $variants;
    }

    public static function getCurrentProductVersion($id, $property, $lang)
    {
        $itemid = self::getProductVersion($id, $property, $lang);
        return $itemid;
    }

    public static function checkQuantity(Request $request, $cart = null, $type = 'product')
    {
        $id = $request->input('id');
        $quantity = $request->input('quantity');
        $properties = $request->input('properties');
        $lang = $request->lang;
        // $properties = explode(',', $properties);

        if(!$quantity OR $quantity == 'null'){
            $quantity = 1;
        }

        /**
         * Check in the DB
         */
        if($properties && $type != 'product'){
            $id = self::getCurrentProductVersion($id, $properties, $lang);
        } else {
            $id = $id;
        }        

        if(env('CHECK_QUANTITY_CUSTOM', false)){
            if($properties && $type != 'product'){
                $product = DB::table('olmo_productitem')->where('locale_hidden_general', $lang)->where('id', $id)->first();
            } else {
                $product = DB::table('olmo_product')->where('locale_hidden_general', $lang)->where('id', $id)->first();
            }              
            $checkQuantity = Quantity::getQuantity($product);
        } else {
            $checkQuantity = Db::table('olmo_quantity')->where('prod_id', $id)->where('type', $type)->first();
        }

        if(!$checkQuantity){
            return false;
        }
        $checkQuantity = json_decode(json_encode($checkQuantity, true), true);
        if(intval($quantity) > intval($checkQuantity['quantity'])){
            return false;
        }

        /**
         * Check in the current session
         */
        if(!$cart){
            $customer = HelpersCustomer::getCustomer($request);
            if(isset($customer['id'])){
                $cart = Db::table('olmo_cart')->where('customer_id', $customer['id'])->where('active', '1')->first();
            } else {
                $cart = Db::table('olmo_cart')->where('guest_id', $customer)->where('active', '1')->first();
            }
            $response = Db::table('olmo_cartitem')->where('cart_id', $cart->id)->where('item_id', $id)->where('properties', $properties)->get();
        } else {
            if(isset($cart->id)){
                if($properties){
                    $response = Db::table('olmo_cartitem')->where('cart_id', $cart->id)->where('item_id', $id)->where('properties', $properties)->get();
                } else {
                    $response = Db::table('olmo_cartitem')->where('cart_id', $cart->id)->where('product_id', $id)->get();
                }                
            } else {
                $response = Db::table('olmo_cartitem')->where('cart_id', $cart)->where('item_id', $id)->where('properties', $properties)->get();
            }
        }

        $response = json_decode(json_encode($response), true);
        $items = 0;
        if($response){
            foreach($response as $item){
                $items += $item['item_qty'];
            }
        }
        $items = $items + intval($quantity);
        if($items > intval($checkQuantity['quantity'])){
            return false;
        }

        return true;

    }

    public static function addQuantity($request)
    {
        $cartitemid = $request->input('cartitemid');
        $quantity = $request->input('quantity');
        $properties = $request->input('properties');

        $product = null;
        $item = null;
        $getProdId = Db::table('olmo_cartitem')->where('id', $cartitemid)->first();

        if($properties != ''){
            $properties = explode(',', $getProdId->properties);
            $properties = json_encode($properties, true);
        }

        if($getProdId){
            if($properties != ''){
                $item = self::getProductItem($getProdId->item_id, $properties);
                $item = $item[0];
                $itemid = $item->id;

                if(env('CHECK_QUANTITY_CUSTOM', false)){
                    $product = Db::table('olmo_productitem')->where('id', $itemid)->first();
                    $checkQuantity = Quantity::getQuantityById($product);
                }else{
                    $checkQuantity = Db::table('olmo_quantity')->where('prod_id', $itemid)->first();
                }

                $product = Db::table('olmo_product')->where('id', $item->postid_hidden_general)->first();
                $product = HelpersSerializer::cleanKeyObj($product);
                $item = HelpersSerializer::cleanKeyObj($item);
            } else {

                $product = Db::table('olmo_product')->where('id', $getProdId->product_id)->first();
                if(env('CHECK_QUANTITY_CUSTOM', false)){
                    $checkQuantity = Quantity::getQuantity($product);
                }else{
                    $checkQuantity = Db::table('olmo_quantity')->where('prod_id', $product->id)->first();
                }
                $product = HelpersSerializer::cleanKeyObj($product);
            }
            if($checkQuantity){
                $checkQuantity = json_decode(json_encode($checkQuantity, true), true);
                if(intval($quantity) <= intval($checkQuantity['quantity'])){
                    Db::table('olmo_cartitem')->where('id', $cartitemid)->update(['item_qty' => $quantity]);
                    if(intval($quantity) < intval($getProdId->item_qty)){
                        return response(['type' => 'Quantity removed', 'current_item' => $item, 'current_product' => $product], 200);
                    } else {
                        return response(['type' => 'Quantity added', 'current_item' => $item, 'current_product' => $product], 200);
                    }
                }
                return response(['No availability'], 419);
            }
            return response(['No product found'], 400);
        }
        return response(['No product found'], 400);

    }

    public static function convertCartFromGuestToCustomer($customer)
    {
        /**
         * Quando un utente guest aggiunge un elemento a carrello e poi fa il login
         * e quell'utente aveva già un carrello attivo con lo stesso prodotto già nel carrello
         * la funzone deve unificare il carrello guest con quello della sessione da loggato e convertire tutti i cart_id del cartitem
         */
        // $cartGuest = Db::table('olmo_cart')->where('guest_id', $customer['x-guest'])->first();
        // Db::table('olmo_cartitem')->where('cart_id', $cartGuest->id)->get();

        $cart = Db::table('olmo_cart')->where('customer_id', $customer['id'])->where('active', '1')->first();
        if($cart){
            $cartGuest = Db::table('olmo_cart')->where('guest_id', $customer['x-guest'])->where('active', '1')->first();
            $getCartItems = Db::table('olmo_cartitem')->where('cart_id', $cartGuest->id)->get();
            foreach($getCartItems as $item){
                Db::table('olmo_cartitem')->where('id', $item->id)->update(['cart_id' => $cart->id]);
            }
            Db::table('olmo_cart')->where('guest_id', $customer['x-guest'])->where('active', '1')->delete();
        } else {
            Db::table('olmo_cart')->where('guest_id', $customer['x-guest'])->where('active', '1')->update(['customer_id' => $customer['id'], 'guest_id' => '']);
        }

    }

    public static function setShippingCostbycart($shippingcost, $shipping, $weight, $countryCart, $provinceCart, $cartid, $shippingMethod)
    {
        $country = $countryCart;
        $provincia = $provinceCart;

        if(isset($shippingMethod['code'])){
            if($shippingMethod['code'] == 'retire'){
                return 0.00;
            }
        }

        /**
         * Performare questa azione solo nella pagina summary e cart
         */

        if(!$shipping){
            $country = $country;
            $provincia = isset($shipping['province']) ? $shipping['province'] : $provincia;
            return $shippingcost;
        } else {
            $path = base_path("app/Containers/Frontend/Delivery/Controllers/Controller.php");
            if(File::exists($path)) {
                $model = ('App\Containers\Frontend\Delivery\Controllers\Controller');
                $price = $model::setDeliveryPrice($weight, $country, $provincia, $cartid);
                return $price;
            }
        }

        return $shippingcost;
    }

    public static function setShippingCostOnCartPage($countryCart, $provinceCart, $weight, $cartid,)
    {
        $country = $countryCart;
        $provincia = $provinceCart;

        $path = base_path("app/Containers/Frontend/Delivery/Controllers/Controller.php");
        if(File::exists($path) AND $countryCart AND $weight AND $cartid) {
            $model = ('App\Containers\Frontend\Delivery\Controllers\Controller');
            $price = $model::setDeliveryPrice($weight, $country, $provincia, $cartid);
            return $price;
        }

        return 0.0;
    }

    public static function setShippingCost($userid, $price, $country, $provincia = null)
    {
        Db::table('olmo_cart')->where('id', $userid)->update(
            [
                'shippingcost' => $price,
                'country' => $country,
                'provincia' => $provincia,
            ]
        );
    }

    public static function getShippingCost($cart)
    {
        $method = Db::table('olmo_shippingmethod')->where('id', $cart->shippingcost)->first();
        if($method){
            $code = $method->code_txt_general;
            if($code == 'retire'){
                return 0.00;
            }
        }
        return $cart->shippingcost;
    }

}
