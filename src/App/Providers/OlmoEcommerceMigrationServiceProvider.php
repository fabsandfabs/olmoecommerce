<?php

namespace Olmo\Ecommerce\App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\File;
use Olmo\Ecommerce\Loaders\OlmoLoadersEcommerceContainers;

class OlmoEcommerceMigrationServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        $this->loadMigrationsFromEcommerceContainers();

    }    

    public function register()
    {
        //
    }

    /**
     * This is something must be changed
     * Load the containers the user create
     */
    public function loadMigrationsFromEcommerceContainers()
    {
        foreach (OlmoLoadersEcommerceContainers::getAllEcommerceContainerPaths() as $containerPath) {
            $containerMigrationPath = $containerPath."/Data/Migrations";       
            if(File::isDirectory($containerMigrationPath)){
                $this->loadMigrationsFrom($containerMigrationPath);
            }            
        }     
    }    
   
}