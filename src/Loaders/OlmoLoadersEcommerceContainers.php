<?php

namespace Olmo\Ecommerce\Loaders;

use Illuminate\Support\Facades\File;
use Illuminate\Support\ServiceProvider;

class OlmoLoadersEcommerceContainers
{

    private const CONTAINERS_DIRECTORY_NAME = 'Containers';  

    // Below is the ecommerce migration tacking action, 
    // user cannot takes control of this cause is the minimum set up to run the Ecommerce system

    public static function getAllEcommerceContainerPaths(): array
    {        
        $sectionNames = self::getEcommerceSectionNames();
        $containerPaths = [];
        foreach ($sectionNames as $name) {
            $sectionContainerPaths = self::getEcommerceSectionContainerPaths($name);
            foreach ($sectionContainerPaths as $containerPath) {
                $containerPaths[] = $containerPath;
            }
        }
        
        return $containerPaths;
    }    
    
    public static function getEcommerceSectionNames(): array
    {
        $sectionNames = [];        
        foreach (self::getEcommerceSectionPaths() as $sectionPath) {
            $sectionNames[] = basename($sectionPath);
        }
    
        return $sectionNames;
    }    
    
    public static function getEcommerceSectionPaths(): array
    {                
        return File::directories(__DIR__ . '/../' . self::CONTAINERS_DIRECTORY_NAME);
    }
    
    public static function getEcommerceSectionContainerPaths(string $sectionName): array
    {        
        return File::directories(__DIR__ . '/../' . self::CONTAINERS_DIRECTORY_NAME . DIRECTORY_SEPARATOR . $sectionName);
    }    

}