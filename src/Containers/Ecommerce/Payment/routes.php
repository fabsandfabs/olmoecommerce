<?php

use Olmo\Ecommerce\Containers\Ecommerce\Payment\Controllers\Controller;
use Illuminate\Support\Facades\Route;

/** Paypal standard*/
Route::post('/{lang}/payment/paypal/auth', [Controller::class, 'paypalAuth']);

Route::post('/{lang}/payment/paypal/order/create/{code}', [Controller::class, 'paypalCreateOrder']);

Route::post('/{lang}/payment/paypal/order/capture-payment/{id}', [Controller::class, 'paypalCapturePayment']);

/** Credi card */
Route::post('/{lang}/payment/credit-card/client-token', [Controller::class, 'clientToken']);

Route::post('/{lang}/payment/credit-card/checkout', [Controller::class, 'checkoutOrder']);