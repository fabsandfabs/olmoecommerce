<?php

namespace Olmo\Ecommerce\Containers\Ecommerce\Payment\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use Olmo\Core\App\Helpers\HelpersSerializer;
use Olmo\Core\App\Helpers\HelpersCustomer;
use Olmo\Ecommerce\App\Helpers\HelpersPayment;
use Olmo\Ecommerce\App\Http\Controller\EcommerceController;

class Controller extends EcommerceController
{

    public function paypalAuth()
    {
        return HelpersPayment::generateAccessTokenPaypal();
    }

    public function clientToken(Request $request)
    {

        $customer = HelpersCustomer::getCustomer($request);

        if (!$customer) {
            return response(['User Session not found'], 403);
        } 

        $lang = $request->lang;
        $clientToken = HelpersPayment::generateClientTokenPaypal($lang, $customer);

        $order = Db::table('olmo_order')->where('customerid_hidden_customer', $customer['id'])->get();
        $order = HelpersSerializer::cleanKeyArray($order);
        $order = end($order);

        $array = array('clientToken' => $clientToken->json(), 'clientId' => env('CLIENT_ID'), 'order' => $order);

        return response($array, $clientToken->status());

    }     
    
    public function paypalCreateOrder(Request $request)
    {
        $lang = $request->lang;
        $code = $request->code;
        
        $order = Db::table('olmo_order')->where('code_read_general', $code)->first();
        $order = json_decode(json_encode($order), true);

        $currency = $order['currency_read_general'];

        return HelpersPayment::createOrderPaypal($order, $currency, $lang);
    }  

    public function checkoutOrder(Request $request)
    {
        if(env('CREDITCARD_METHOD') == 'paypal'){

            $lang = $request->lang;
            $expiry = $request->input('expiry'); 
            $name = $request->input('name');  
            $number = $request->input('number');
            $security = $request->input('security');
            $code = $request->input('code');
    
            $order = Db::table('olmo_order')->where('code_read_general', $code)->first();            
            $order = HelpersSerializer::cleanKeyObj($order);

            $paypal = HelpersPayment::checkoutOrderPaypalCreditCard($order, $lang, $expiry, $name, $number, $security);            

            $theOrder = [
                'paypal' => $paypal->json(),
                'order' => $order
            ];

            return response($theOrder, $paypal->status());
        }
    } 

    public function paypalCapturePayment(Request $request)
    {
        $id = $request->id;
        return HelpersPayment::capturePaymentPaypal($id);
    }    

}
