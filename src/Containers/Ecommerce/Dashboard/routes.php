<?php

use Olmo\Ecommerce\Containers\Ecommerce\Dashboard\Controllers\Controller;
use Illuminate\Support\Facades\Route;

/** Shopping Cart */
// Route::post('lean/shoppingcart/version', [Controller::class, 'testGetProductVersion']);
Route::get('ecommerce/dashboard/example', [Controller::class, 'exampleRoute']);
Route::post('ecommerce/dashboard/income', [Controller::class, 'Income']);
Route::post('ecommerce/dashboard/purchase', [Controller::class, 'getPercentageIncome']);
Route::get('ecommerce/dashboard/graph', [Controller::class, 'getGraphIncome']);
Route::get('ecommerce/dashboard/customers', [Controller::class, 'getCustomersIncome']);
Route::get('ecommerce/dashboard/latestorders/{number}', [Controller::class, 'getLatestOrders']);
Route::get('ecommerce/dashboard/agentlist', [Controller::class, 'getAgentList']);
// Route::post('{lang}/shoppingcart', [Controller::class, 'addCart']);
// Route::delete('{lang}/shoppingcart', [Controller::class, 'deleteCartItem']);
// Route::post('{lang}/shoppingcart/quantity', [Controller::class, 'addQuantity']);
