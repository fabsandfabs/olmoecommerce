<?php

namespace Olmo\Ecommerce\Containers\Ecommerce\Dashboard\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use Olmo\Ecommerce\App\Http\Controller\EcommerceController;
use Olmo\Core\App\Helpers\HelpersUser;
use Olmo\Core\App\Helpers\HelpersAgent;
use Olmo\Core\App\Helpers\HelpersFilemanager;
use Olmo\Core\App\Helpers\HelpersSerializer;
use Olmo\Core\App\Helpers\HelpersExtra;
use Olmo\Ecommerce\App\Helpers\HelpersCart;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Log;

class Controller extends EcommerceController
{
    public function Income(Request $request){
        $agent = HelpersAgent::verifyAgent($request);
        if($agent == false){
            return response('forbidden', 403);
        }

        $rules = [
            "to" => "required|integer",
            "from" => "required|integer",
            "performance_compare" => "required|boolean"
        ];

        $validationResult = HelpersExtra::validateInput($request, $rules);
        if ($validationResult['success'] === false) {
            return response()->json($validationResult, 422);
        }

        ['to' => $to, 'from' => $from, 'performance_compare' => $compare] = $validationResult['data'];

        $incomes = self::getIncomeByTime($agent, $to, $from, $compare);
        return response()->json($incomes, 200);
    }

    public function getCustomersIncome(Request $request){
        $agent = HelpersAgent::verifyAgent($request);
        if($agent == false){
            return response('forbidden', 403);
        }
        $list = [];
        if($agent->role == "agent"){
            $customers = DB::table('olmo_customer')->where('agent_agent_general', $agent->id)->get();
            $emails = [];
            foreach($customers as $customer){
                $emails[] = $customer->email_email_general;
            }
            $list = DB::select("select SUM(total_read_general) as total, customer_read_general as email from olmo_order where customer_read_general IN ? group by customer_read_general", [$emails]);
        }else{
            $list = DB::select("select SUM(total_read_general) as total, customer_read_general as email from olmo_order group by customer_read_general");
        }

        return response()->json($list, 200);
    }

    public function getLatestOrders(Request $request, $number){
        $agent = HelpersAgent::verifyAgent($request);
        if($agent == false){
            return response('forbidden', 403);
        }

        if($agent->role == "agent"){
            $customers = DB::table('olmo_customer')->where('agent_agent_general', $agent->id)->get();
            $emails = [];
            foreach($customers as $customer){
                $emails[] = $customer->email_email_general;
            }
            $orders = DB::table('olmo_order')->where('orderstatus_select_general', 'completed')->whereIn('customer_read_general', $emails)->limit($number)->get();
        }else{
            $orders = DB::table('olmo_order')->where('orderstatus_select_general', 'completed')->limit($number)->get();
        }

        $list = [];
        foreach($orders as $order){
            $list[] = ['ordercode' => $order->name_txt_general, 'total' => $order->total_read_general, "customer" => $order->customer_read_general ];
        }

        return response()->json($list, 200);

    }

    public function getAgentList(Request $request){
        $agent = HelpersAgent::verifyAgent($request);
        if($agent == false){
            return response('forbidden', 403);
        }
        if($agent->role == 'agent'){
            return response('forbidden', 403);
        }

        // $agent_and_customer_list = DB::select("SELECT concat(id), agent_agent_general FROM olmo_customer WHERE agent_agent_general != '' GROUP BY agent_agent_general ");
        $agent_and_customer_list = DB::table('olmo_customer')
        ->selectRaw("GROUP_CONCAT(email_email_general) as customers_email, agent_agent_general as id")
        ->where('agent_agent_general', '!=', '')
        ->groupBy('agent_agent_general')
        ->get();

        $agent_incomes = [];
        foreach($agent_and_customer_list as $agent){
            $list_customer_email = explode(',',$agent->customers_email);
            $agent_info = HelpersSerializer::cleanKeyObj( HelpersUser::getUserById($agent->id) );
            if($agent_info['avatar'] != ''){
                $agent_info['avatar'] = HelpersFilemanager::getSinglePathById($agent_info['avatar']);
                if($agent_info['avatar'] != ''){
                    if( isset($agent_info['avatar']['original']) ){
                        if( isset($agent_info['avatar']['original']['compressed']) ){
                            $agent_info['avatar'] = $agent_info['avatar']['original']['compressed'];
                        }else if(isset($agent_info['avatar']['original']['src'])){
                            $agent_info['avatar'] = $agent_info['avatar']['original']['src'];
                        }
                    } 
                }
            }

            $income_per_customer = [];
            $total_income = 0.00;
            foreach($list_customer_email as $email){
                $income = DB::table('olmo_order')
                ->selectRaw("SUM(total_read_general) as total")
                ->where('orderstatus_select_general', 'completed')
                ->where('customer_read_general', $email)
                ->first();

                $income_per_customer[$email] = $income->total;
                $total_income += $income->total;
            }
            
            $agent_incomes[(string) $total_income] = array_merge($agent_info);//, $income_per_customer);
        }

        krsort($agent_incomes, SORT_NUMERIC);
        $i = 1;
        foreach($agent_incomes as $total => &$agent){
            $agent['rank'] = "top_".$i;
            $agent['total'] = "€".$total;
            $i++;
        }
        
        // return response()->json([$agents, $income_per_customer, ], 200);
        return response()->json(array_values($agent_incomes), 200);
    }

    public function getPercentageIncome(Request $request){
        $agent = HelpersAgent::verifyAgent($request);
        if($agent == false){
            return response('forbidden', 403);
        }
        $rules = [
            "to" => "required|integer",
            "from" => "required|integer"
        ];

        $validationResult = HelpersExtra::validateInput($request, $rules);
        if ($validationResult['success'] === false) {
            return response()->json($validationResult, 422);
        }
        ['to' => $to, 'from' => $from] = $validationResult['data'];

        
        $income_frontend = self::getIncomeQuery($agent, $to, $from, 0);
        $income_backoffice = self::getIncomeQuery($agent, $to, $from, 1);

        $total = $income_frontend + $income_backoffice;
        $percentage_frontend = round(( 100 * $income_frontend ) / $total, 2); 
        $percentage_backoffice = round(( 100 * $income_backoffice ) / $total, 2); 

        return response()->json([
            "frontend" => ['value' => $income_frontend, 'percentage' => $percentage_frontend],
            "backoffice" => ['value' => $income_backoffice, 'percentage' => $percentage_backoffice]
        ], 200);
    }

    public function getGraphIncome(Request $request){
        $agent = HelpersAgent::verifyAgent($request);
        if($agent == false){
            return response('forbidden', 403);
        }

        $current_year = [];
        for($to = 0; $to < 12; $to++){
            $from = $to;
            $current_year[] = self::getIncomeQuery($agent, $to, $from, 2, "graph");
        }

        $last_year = [];
        for($to = 12; $to < 24; $to++){
            $from = $to;
            $last_year[] = self::getIncomeQuery($agent, $to, $from, 2, "graph");
        }

        return response()->json([
            ["name" => "Current Year", "data" => array_reverse($current_year)],
            ["name" => "Last Year", "data" => array_reverse($last_year)]
        ], 200);
    }

    public static function calcPercentage($income, $income_compare, $add_symbol = false){
        $percentage = round( $income != 0 AND $income_compare != 0 ) ? round( (100 * $income / $income_compare) - 100, 2 ) : 0;
        return $percentage;
    }

    public static function getIncomeQuery($agent, $to, $from, $fromBackoffice, $type = "single", $status = "completed"){
        $orders = DB::table('olmo_order')->select([
            'createdonutc_read_general as order_date',
            'customerid_hidden_customer as customer_id',
            'createdby_read_information as agent_email',
            'total_read_general as total',
        ]);

        if($agent->role == "agent"){
            $orders = $orders->where('createdby_read_information', $agent->email_email_general);
        }
        switch($fromBackoffice){
            case "0":
                $orders = $orders->where('backoffice_hidden_general', "=", 0); // ordini from the customer frontend 
                break;

            case "1":
                $orders = $orders->where('backoffice_hidden_general', "=", 1); // order from backoffice
                break;
            
            default:
        }
        if($type == "single"){
            if($from == 1 && $to == 0){
                $orders = $orders->where(DB::raw("DATE_FORMAT(createdonutc_read_general, '%Y-%m')"), '=', DB::raw("DATE_FORMAT(CURDATE(), '%Y-%m')"));
            } if($from == 12 && $to == 0){
                $orders = $orders->where(DB::raw("DATE_FORMAT(createdonutc_read_general, '%Y')"), '=', DB::raw("DATE_FORMAT(CURDATE(), '%Y')"));
            }
        }else if($type == "graph"){
            if($from == 0){
                $orders = $orders->where(DB::raw("DATE_FORMAT(createdonutc_read_general, '%Y-%m')"), '=', DB::raw("CONCAT(DATE_FORMAT(CURDATE(), '%Y'), '-12')") );  
            }else{
                $orders = $orders->where(DB::raw("DATE_FORMAT(createdonutc_read_general, '%Y-%m')"), '=', DB::raw("DATE_FORMAT(DATE(CONCAT(DATE_FORMAT(CURDATE(), '%Y'), '-12-01')) - INTERVAL 1 MONTH, '%Y-%m')") );
            }
            
        }
        // if($to > 0){
        //     $orders = $orders->whereBetween(DB::raw('DATE(createdonutc_read_general)'), [DB::raw("DATE(NOW() - INTERVAL $from MONTH)"), DB::raw("DATE(NOW() - INTERVAL $to MONTH)")]);
        // }else{
        //     $orders = $orders->whereBetween(DB::raw('DATE(createdonutc_read_general)'), [DB::raw("DATE(NOW() - INTERVAL $from MONTH)"), DB::raw('DATE(NOW())')]);
        // }
        $orders = $orders
            ->where('orderstatus_select_general', '=', $status)
            ->get();
        // return $orders;
        $income = 0;
        foreach($orders as $order){
            $income += $order->total;
        }

        return $income;
    }

    public static function getIncomeByTime($agent, $to = "0", $from = "1", $performance_compare = false , $status = 'completed' ){
        /**
         * @ indica le colonne
         * 
         * Dentro olmo_order
         * per customer @customerid_hidden_customer
         * per currency @currency_read_general
         * chi ha creato l'ordine? @createdby_read_information
         * per la data in utc? @createdonutc_read_information
         * total_read_general => totale
         * 
         * Dentro olmo_customer
         * per agent (id) @agent_agent_general
         * per l'email customer (id) @email_email_general
         * 
         */

        // $select = "SELECT createdonutc_read_general as order_date, customerid_hidden_customer as customer_id, createdby_read_information as agent_email, total_read_general as total ";
        // $from = "FROM olmo_order";
        // $where_date = "WHERE DATE(createdonutc_read_general) between DATE( NOW() - interval $month month ) and DATE( NOW() ) ";
        // $where_status = "AND orderstatus_select_general = 'completed'";
        // $orders = DB::select("
         
        // ");

        $results = [ "current" => [ "tot" => "€0", "to" => $to, "from" => $from ] ];

        $income = self::getIncomeQuery($agent, $to, $from, 2); // TODO: vanno inclusi i valori a FE? se si 2
        // return response($income, 200);
        $results['current']['tot'] = $income;

        if($performance_compare){

            $orders_differents = DB::table('olmo_order')
            ->select([
                'createdonutc_read_general as order_date',
                'customerid_hidden_customer as customer_id',
                'createdby_read_information as agent_email',
                'total_read_general as total',
            ]);
            
            if($agent->role == "agent"){
                $orders_differents = $orders_differents->where('createdby_read_information', $agent->email_email_general);
            }

            if($from >= 12){
                $to_compare = $to + 12;
                $from_compare = $from + 12;
            }else{
                $to_compare = $to + 1;
                $from_compare = $from + 1;
            }

            if($from == 1){
                $orders_differents = $orders_differents->where(DB::raw("DATE_FORMAT(createdonutc_read_general, '%Y-%m')"), '=', DB::raw("DATE_FORMAT(CURDATE() - INTERVAL 1 MONTH, '%Y-%m')"));
            }
            if($from == 12){
                $orders_differents = $orders_differents->where(DB::raw("DATE_FORMAT(createdonutc_read_general, '%Y')"), '=', DB::raw("DATE_FORMAT(CURDATE() - INTERVAL 1 YEAR, '%Y')"));
            }
            // $orders_differents = $orders_differents->whereBetween(DB::raw('DATE(createdonutc_read_general)'), [DB::raw("DATE(NOW() - INTERVAL $from_compare MONTH)"), DB::raw("DATE(NOW() - INTERVAL $to_compare MONTH)")]);

            $orders_differents = $orders_differents
                ->where('orderstatus_select_general', '=', $status)
                ->get();
            
            $income_compare = 0;
            foreach($orders_differents as $order){
                $income_compare += $order->total; 
            } 
            
            $percentage = self::calcPercentage($income, $income_compare, true);

            $results['comparison'] = [ "tot" => $income_compare, "to" => $to_compare, "from" => $from_compare, "percentage_difference" =>  $percentage];
            // TODO: gestisci moneta

        }

        return $results;
        
        
    }
    
}
