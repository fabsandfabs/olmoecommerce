<?php

use Olmo\Ecommerce\Containers\Ecommerce\Wishlist\Controllers\Controller;
use Illuminate\Support\Facades\Route;

Route::get('/{lang}/wishlist', [Controller::class, 'getWishlist']);

Route::get('/wishlist', [Controller::class, 'getWishlist']);

Route::post('/wishlist', [Controller::class, 'addWishlist']);

Route::delete('/wishlist', [Controller::class, 'deleteWishlist']);