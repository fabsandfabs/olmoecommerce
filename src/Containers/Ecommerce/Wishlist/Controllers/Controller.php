<?php

namespace Olmo\Ecommerce\Containers\Ecommerce\Wishlist\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use Olmo\Ecommerce\App\Http\Controller\EcommerceController;
use Olmo\Core\App\Helpers\HelpersCustomer;
use Olmo\Core\App\Helpers\HelpersSerializer;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Cache;

class Controller extends EcommerceController
{

    public function getWishlist(Request $request)
    {
        /**
         * Qui va aggiunta la logica della lingua
         */
        // $lang = $request->lang;

        $customer = HelpersCustomer::getCustomer($request);
        if (!$customer) {
            return response(['User Session not found'], 403);
        }
        $customerid = $customer['id'];

        $cacheKey = md5($customerid.'-customer');
        if(Cache::has($cacheKey)) {
            return Cache::get($cacheKey);
        }

        $response = [];
        $product = [];
        $items = Db::table('olmo_wishlist')->where('customer_id', $customerid)->get();
        $i  = 0;
       
        foreach ($items as $item) {
            $table = 'olmo_'.$item->model_name;
            $model = Db::table($table)->where('id', $item->product_id)->first();
            $product[$i] = HelpersSerializer::routeResponse($model);
            $i++;
        }
        
        $response = $product;
        
        Cache::put($cacheKey, $response, now()->addMinutes(10));

        return $response;
    }

    public function addWishlist(Request $request)
    {
        $model = $request->input('type');
        $productid = $request->input('id');

        $customer = HelpersCustomer::getCustomer($request);
        if (!$customer) {
            return response(['User Session not found'], 403);
        }
        $customerid = $customer['id'];

        if (!Schema::hasTable('olmo_'.$model)){
            return response(['Table not found'], 403);
        }

        $item = [
            'product_id' => $productid,
            'customer_id' => $customerid,
            'model_name' => $model
        ];

        $insertitem = Db::table('olmo_wishlist')->insertGetId($item);

        if($insertitem){
            $cacheKey = md5($customerid.'-customer');
            if(Cache::has($cacheKey)) {
                Cache::forget($cacheKey);
            }
            $response = $this->getWishlist($request);
            Cache::put($cacheKey, $response, now()->addMinutes(10));
            return response('', 200);
        } 
        return response('', 400);
    }

    public function deleteWishlist(Request $request)
    {
        $productid = $request->input('id');
        $model = $request->input('type');

        $customer = HelpersCustomer::getCustomer($request);
        if (!$customer) {
            return response(['User Session not found'], 403);
        }
        $customerid = $customer['id'];
        
        $response = Db::table('olmo_wishlist')->where('model_name', $model)->where('product_id', $productid)->where('customer_id', $customerid)->delete();
    
        if($response){
            $cacheKey = md5($customerid.'-customer');
            if(Cache::has($cacheKey)) {
                Cache::forget($cacheKey);
            }
            $response = $this->getWishlist($request);
            Cache::put($cacheKey, $response, now()->addMinutes(10));
            return response('', 200);
        } 
        return response('', 400);
    }

}
