<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCartsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('olmo_cart', function (Blueprint $table) {
            // Create new table...
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';

            $table->increments('id')->unsigned();
            $table->text('customer_id')->nullable();
            $table->text('guest_id')->nullable();
            $table->text('shippingmethod')->nullable();
            $table->text('privincia')->nullable();
            $table->text('paymentgmethod')->nullable();
            $table->text('shippingaddress')->nullable();
            $table->text('billingaddress')->nullable();
            $table->decimal('shippingcost', $precision = 12, $scale = 2)->nullable();
            $table->text('country')->nullable();
            $table->integer('order_id')->nullable();

            $table->tinyInteger('active')->nullable();
            $table->tinyInteger('backoffice')->nullable();
            $table->tinyInteger('discount')->nullable();
            
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('olmo_cart');
    }
}
