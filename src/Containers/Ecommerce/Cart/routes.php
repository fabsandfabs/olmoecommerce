<?php

use Olmo\Ecommerce\Containers\Ecommerce\Cart\Controllers\Controller;
use Illuminate\Support\Facades\Route;

/** Shopping Cart */
// Route::post('lean/shoppingcart/version', [Controller::class, 'testGetProductVersion']);
Route::get('lean/shoppingcart', [Controller::class, 'getCart']);
Route::post('{lang}/shoppingcart', [Controller::class, 'addCart']);
Route::delete('{lang}/shoppingcart', [Controller::class, 'deleteCartItem']);
Route::post('{lang}/shoppingcart/quantity', [Controller::class, 'addQuantity']);
Route::get('{lang}/shoppingcart', [Controller::class, 'getCartComplete']);

Route::post('{lang}/shoppingcart/discount', [Controller::class, 'setDiscount']);
Route::delete('{lang}/shoppingcart/discount', [Controller::class, 'delDiscount']);

Route::post('{lang}/shoppingcart/addresses', [Controller::class, 'setAddress']);
// Route::post('{lang}/shoppingcart/addresses/shipping', [Controller::class, 'setAddressShipping']);
// Route::post('{lang}/shoppingcart/addresses/billing', [Controller::class, 'setAddressBilling']);
Route::post('{lang}/shoppingcart/shippingmethod', [Controller::class, 'setShippingmethod']);
Route::post('{lang}/shoppingcart/paymentmethod', [Controller::class, 'setPaymentmethod']);

Route::get('{lang}/shoppingcart/cartid', [Controller::class, 'getCartIdTesting']);