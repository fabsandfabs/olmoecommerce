<?php

namespace Olmo\Ecommerce\Containers\Ecommerce\Cart\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

use Olmo\Ecommerce\App\Http\Controller\EcommerceController;
use Olmo\Core\App\Helpers\HelpersCustomer;
use Olmo\Core\App\Helpers\HelpersLang;
use Olmo\Core\App\Helpers\HelpersSerializer;
use Olmo\Ecommerce\App\Helpers\HelpersCart;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Log;



class Controller extends EcommerceController
{

    public function delDiscount(Request $request)
    {
        $customer = HelpersCustomer::getCustomer($request);
        if(is_array($customer)){
            Db::table('olmo_cart')->where('customer_id', $customer['id'])->update(['discount' => '']);
        } else {
            Db::table('olmo_cart')->where('guest_id', $customer)->update(['discount' => '']);
        }  
        
        return response([], 200);
    }

    public function setDiscount(Request $request)
    {
        $code = $request->input('code');
        $customer = HelpersCustomer::getCustomer($request);
        $discount = self::discountCondition($code, $customer);
        if(!$discount){
            return response([], 404);
        }        
        if(is_array($customer)){
            Db::table('olmo_cart')->where('customer_id', $customer['id'])->update(['discount' => $code]);
        } else {
            Db::table('olmo_cart')->where('guest_id', $customer)->update(['discount' => $code]);
        }  
        
        return response([], 200);
    }

    public function testGetProductVersion(Request $request)
    {
        $id = $request->input('id'); 
        $properties = $request->input('properties');
        $lang = $request->input('lang');
        return HelpersCart::getProductVersion($id, $properties, $lang);
    }

    public static function createEmptyCart($customer_id, $active){
        // TODO: da aggiungere default del customer? O rendere la cosa parametrizzabile?
        return Db::table('olmo_cart')->insertGetId([
            'customer_id' => $customer_id,
            'active' => $active
        ]);
    }

    public static function getCartId($customer, $orderid_from_backoffice = null)
    {
        if(!is_array($customer) OR @$customer['guest_id']){
            $cart = self::cartForGuest($customer);
        } else if($orderid_from_backoffice != null) {
            $cart = self::getCartFromOrderId($orderid_from_backoffice);
        } else {            
            $cart = self::cartForCustomer($customer, $orderid_from_backoffice);
        }
        return $cart;
    }

    public static function cartForGuest($customer) 
    {
        $guest_id = $customer['guest_id'] ?? $customer;
        $cart = Db::table('olmo_cart')->where('guest_id', $guest_id)->where('active', '1')->first();
        if(!$cart){
            return null;
        }            
        return $cart;
    }

    public static function getCartFromOrderId($orderid){
        $cart = Db::table('olmo_cart')->where('order_id', $orderid)->where('active', '1')->where('backoffice', 1)->get();
        
        if(count($cart) == 0){
            $cart = [];
        } else if(count($cart) == 1){
            $cart = $cart[0];
        } else {
            $singleCart = [];
            foreach($cart as $id){
                array_push($singleCart, $id->id);
            }
            $cart = $cart[0];
            $cart->id = $singleCart;
        }

        return $cart;
    }

    public static function cartForCustomer($customer, $backofficeOrder = null) 
    {
        $id = isset($customer['id']) ? $customer['id'] : $customer['customer_id'];
        $backofficeVal = $backofficeOrder ? 1 : null;
        $cart = Db::table('olmo_cart')->where('customer_id', $id)->where('active', '1')->where('backoffice', $backofficeVal)->get();

        if(count($cart) == 0){
            $cart = [];
        } else if(count($cart) == 1){
            $cart = $cart[0];
        } else if($cart) {
            $singleCart = [];
            foreach($cart as $id){
                array_push($singleCart, $id->id);
            }
            $cart = $cart[0];
            $cart->id = $singleCart;
        }

        return $cart;
    }    

    public static function getCartByCustomer(Request $request)
    {
        $customer = HelpersCustomer::getCustomer($request);
        
        if(isset($customer['x-guest'])){
            //TODO: working on make customer purchase possible
            // HelpersCart::convertCartFromGuestToCustomer($customer);
        }
        if($request->has('orderid')){
            return self::getCartId($customer, $request->input('orderid'));
        }

        return self::getCartId($customer);
    }

    public function getCart(Request $request)
    {
        return self::getPartialCart($request);
    }

    public static function getPartialCart(Request $request, $backofficeOrder = null)
    {               
        $cart = self::getCartByCustomer($request, $backofficeOrder);
        return self::getPartialCartProcess($cart);
    }

    public static function getPartialCartProcess($cart){        
        $items = [];
        if($cart){
            if(is_array($cart->id)){ 
                $items = Db::table('olmo_cartitem')->whereIn('cart_id', $cart->id)->get();
            } else {
                $items = Db::table('olmo_cartitem')->where('cart_id', $cart->id)->get();
            }
            // return $items;
            $qty = [];
            $products = [];            
            foreach($items as $item){
                $product = Db::table('olmo_product')->where('id', $item->item_id)->first();
                $singleitame = null;
                if($item->properties) {
                    $singleitame = Db::table('olmo_productitem')->where('id', $item->item_id)->first();
                    $singleitame = HelpersSerializer::cleanKeyObj($singleitame);
                }
                $product = HelpersSerializer::cleanKeyObj($product);
                $product['id'] = $item->id;
                $product['cartitemid'] = $item->id;
                $product['type'] = '';
                if($singleitame){
                    $product['version'] = isset($singleitame['name']) ? $singleitame['name'] : $product['name'];
                } else {
                    $product['version'] = '';
                }
                $product['quantity'] = $item->item_qty;
                array_push($qty, (int)$item->item_qty);
                array_push($products, $product);
            }        

            $response['cart_id'] = isset($cart->id) ? $cart->id : false;
            $response['customer_id'] = isset($cart->customer_id) ? $cart->customer_id : null;
            $response['guest_id'] = isset($cart->guest_id) ? $cart->guest_id : null;
            $response['items'] = $products;
            $response['country'] = $cart->country;
            $response['provincia'] = $cart->provincia;
            $response['items_quantity'] = array_sum($qty);
            $response['items_quantity_count'] = count($items);
    
            return $response;            
        }
    }

    public static function getCartShippingMethod($cart)
    {
        $item = Db::table('olmo_cart')->where('id', $cart['cart_id'])->first();
        $method = Db::table('olmo_shippingmethod')->where('id', $item->shippingmethod)->first();

        return HelpersSerializer::cleanKeyObj($method);
    }

    public static function getCartPaymentMethod($cart)
    {
        $item = Db::table('olmo_cart')->where('id', $cart['cart_id'])->first();
        $method = Db::table('olmo_paymentmethod')->where('id', $item->paymentmethod)->first();

        return HelpersSerializer::cleanKeyObj($method);
    }

    public static function getShippingMethod()
    {
        $shipping = Db::table('olmo_shippingmethod')->where('enabled_is_general', 'true')->get();
        return HelpersSerializer::cleanKeyArray($shipping);
    }    

    public static function getPaymentMethod()
    {
        $payment = Db::table('olmo_paymentmethod')->where('enabled_is_general', 'true')->get();
        return HelpersSerializer::cleanKeyArray($payment);
    }    

    public static function setAddress(Request $request)
    {
        $shipping = $request->input('shipping');
        $billing = $request->input('billing');
        $cartId = $request->input('cart_id');
        $xtoken =  $request->hasHeader('x-token');
        $guesttoken = $request->hasHeader('x-guest');        

        $customer = HelpersCustomer::getCustomer($request);
        if (!$customer) {
            return response(['User Session not found'], 403);
        }
        if(is_array($customer)){
            $customerid = $customer['id'];
        } else {
            $customerid = $customer;
        }           

        if(!$shipping){
            return response('Shipping code missing', 400);
        }
        if(!$billing){
            return response('Billing code missing', 400);
        }
        if(!$cartId){
            return response('CartID missing', 400);
        }

        if($customer){
            if($shipping != '' AND $billing != ''){
                $cart = DB::table('olmo_cart')->where('id', $cartId)->where('active', '1')->first();
                if($cart){
                    $items = DB::table('olmo_cart')->where('customer_id', $customerid)->where('active', '1')->
                    update(['shippingaddress' => $shipping, 'billingaddress' => $billing, 'shippingcost' => 0.00]);
                    if(!$items){
                        $items = DB::table('olmo_cart')->where('guest_id', $customerid)->where('active', '1')->
                        update(['shippingaddress' => $shipping, 'billingaddress' => $billing, 'shippingcost' => 0.00]);                        
                    }
                    return response([
                        // 'cart' => $items,
                        'customerid' => $customerid,
                        'shipping' => $shipping, 
                        'billing' => $billing,
                        'cart' => $cart
                    ], 200);                    
                }
                return response([
                    'cart' => $cart
                ], 403);
            } else {
                return response([
                    'customer' => $customer, 
                    'shipping' => $shipping, 
                    'billing' => $billing,
                    'xtoken' => $xtoken,
                    'guesttoken' => $guesttoken,
                ], 403);    
            }
        } else {
            return response([
                'customer' => $customer, 
                'shipping' => $shipping, 
                'billing' => $billing,
                'xtoken' => $xtoken,
                'guesttoken' => $guesttoken,
            ], 403);    
        }

        // if($items){
        return response([$items], 200);
        // }else{
        //     return response(['Error please refresh the page', $customer['id'], $shipping, $billing],400);
        // }
    }    

    public static function setShippingmethod(Request $request)
    {
        $code = $request->input('code');
        $cartId = $request->input('cart_id');
        $method = DB::table('olmo_shippingmethod')->where('code_txt_general', $code)->first();

        if(!$cartId){
            return response('cartID missing', 400);
        }
        if(!$code){
            return response('code missing', 400);
        }

        
        DB::table('olmo_cart')->where('id', $cartId)->update(['shippingmethod' => $method->id]);

        if($code == 'retire'){
            $updatePrice = DB::table('olmo_cart')->where('id', $cartId)->update(['shippingcost' => $method->cost_txt_general]);
            if($updatePrice){
                return response(['id' => $cartId, 'code' => $code, 'updatePrice' => $updatePrice], 200);
            }
            // return response([$cart->id, $code], 400);
        }

        return response([$cartId, $code], 200);
    }

    public static function setPaymentmethod(Request $request)
    {
        $code = $request->input('code');
        $cartId = $request->input('cart_id');

        if(!$cartId){
            return response('cartID missing', 400);
        }
        if(!$code){
            return response('code missing', 400);
        }

        $method = DB::table('olmo_paymentmethod')->where('code_txt_general', $code)->first();
        $response = DB::table('olmo_cart')->where('id', $cartId)->update(['paymentmethod' => $method->id]);

        return response([$response], 200);
    }    

    public static function setShippingAddress($cart)
    {
        $method = DB::table('olmo_cart')->where('id', $cart['cart_id'])->where('customer_id', $cart['customer_id'])->first();
        if(!$method){
            $method = DB::table('olmo_cart')->where('id', $cart['cart_id'])->where('guest_id', $cart['guest_id'])->first();
        }
        return $method->shippingaddress;
    }

    public static function getCustomerItem($id)
    {
        if($id){
            $address = DB::table('olmo_customeritem')->where('id', $id)->first();
            $address = HelpersSerializer::cleanKeyObj($address);
            return $address;
        }
        return null;
    }    

    public static function setBillingAddress($cart)
    {
        $method = DB::table('olmo_cart')->where('id', $cart['cart_id'])->where('customer_id', $cart['customer_id'])->first();
        if(!$method){
            $method = DB::table('olmo_cart')->where('id', $cart['cart_id'])->where('guest_id', $cart['guest_id'])->first();
        }
        return $method->billingaddress;
    }    

    public function getCartComplete(Request $request)
    {        
        return self::getFullCart($request);
    }

    public static function getFullCart($request, $backofficeOrder = null)
    {
        $getPartialCart = self::getPartialCart($request, $backofficeOrder);   

        $customer = $getPartialCart;        
        $lang = $request->lang;        
        
        $response  = [];        
        $cart = self::getCartProperty($customer, $lang, $request->input('orderid', null));
        
        //TODO: 
        /**
         * Qui è necessario capire perchè c'è una o più chiamate in cui l'utente non viene autenticato e il carrello di conseguenza non viene trovato
         * questa cosa risulta molto strana perchè il flusso non genera un errore a front, quindi va individuato in che punto e in che momento viene fatta la request
         * la rotta che viene chiamata è sempre:
         * Route::get('{lang}/shoppingcart', [Controller::class, 'getCartComplete']);
         */        
        if(!$cart){
            return [];
        } else if(!$customer){
            return [];
        }
        $backoffice = $backofficeOrder ? 1 : null;
        if(isset($customer['guest_id'])){
            $cartdb = Db::table('olmo_cart')->where('guest_id', $customer['guest_id'])->where('active', '1')->where('backoffice', $backoffice)->first();
        } else {
            $cartdb = Db::table('olmo_cart')->where('customer_id', $customer['customer_id'])->where('active', '1')->where('backoffice', $backoffice)->first();
        }
        if(isset($customer['guest_id'])){
            $response['x-guest'] = $customer['guest_id'];
        }

        $response['cart_country'] = $cartdb->country;
        $response['cart_provincia'] = $cartdb->provincia;
        
        $response['currency']['code'] = 'EUR';
        $response['currency']['symbol'] = '€';
        
        $response['addresses'] = [
            "shipping" => self::setShippingAddress($getPartialCart),
            "billing"  => self::setBillingAddress($getPartialCart)
        ];        

        $response['addressesextended'] = [
            "shipping" => self::getCustomerItem($response['addresses']['shipping']),
            "billing"  => self::getCustomerItem($response['addresses']['billing'])
        ];
            
        
        $response['id'] = $getPartialCart['cart_id'];
        $response['country'] = isset($response['addressesextended']["shipping"]['country']) ? $response['addressesextended']["shipping"]['country'] : $getPartialCart['country'];
        $response['provincia'] = isset($response['addressesextended']["shipping"]['provincia']) ? $response['addressesextended']["shipping"]['provincia'] : $getPartialCart['provincia'];
                    
        $response['selectedshippingmethod'] = self::getCartShippingMethod($getPartialCart);
        $response['selectedpaymentmethod'] = self::getCartPaymentMethod($getPartialCart);
 
        $response['shippingmethods'] = self::getShippingMethod();
        $response['paymentmethods'] = self::getPaymentMethod();

        // $response['metainfo'] = json_decode($customer->meta_hidden_general);        
        $response['weight'] = floatval(number_format($cart['weight'], 2, '.', ''));

        /** 
         * The Shipping cost is performed in case the shipping addess has been chosen
         * otherwise it takes the value setted during the process calculation of shipping cost in the cart page  
         */
        $shippigcost = HelpersCart::setShippingCostbycart(
                            number_format($cart['shippingcost'], 2, '.', ''), 
                            $response['addresses']["shipping"], 
                            $response['weight'], 
                            $response['country'], 
                            $response['provincia'], 
                            $response['id'],
                            $response['selectedshippingmethod']
                        );
        $shippigcostcart = HelpersCart::setShippingCostOnCartPage(
                            $response['cart_country'], 
                            $response['cart_provincia'],
                            $response['weight'],
                            $response['id']
                        );

        $response['shippingcost'] = $shippigcost;
        $response['shippingcostcart'] = $shippigcostcart;
        $response['subtotalcart'] = number_format($cart['subtotal'], 2, '.', '');
        $response['totalcart'] = number_format($cart['total'], 2, '.', '') + $shippigcostcart;        
        $response['subtotal'] = number_format($cart['subtotal'], 2, '.', '');

        $response['items'] = $cart['items'];
        $response['items_quantity'] = $getPartialCart['items_quantity'];
        $response['items_quantity_count'] = $getPartialCart['items_quantity_count'];

        $discountCode = self::getDiscountCode($cartdb);

        $response['discountcode'] = $discountCode;
        $response['discount'] = self::getCustomerDiscount($discountCode, number_format($cart['total'], 2, '.', ''), $cart['items'], $customer);
        $response['total'] = number_format($cart['total'], 2, '.', '') + $shippigcost - $response['discount'];
        // $response['total_vat'] = number_format($cart['total'], 2, '.', '');
        $response['taxes'] = env('TAXES');

        $response['currencies'] = [];

        return $response;

    }

    public static function getDiscountCode($cartdb){
        return $cartdb->discount;
    }    

    public static function discountCondition($code, $customer){
        $discount = Db::table('olmo_discount')->where('enabled_is_general', 'true')->where('code_txt_general', $code)->first();
        if(!$discount){
            return 0;
        }

        $now = Carbon::now();

        $startDate = str_replace("T", " ", $discount->startdate_date_general);
        $startDate = str_replace(".000Z", "", $startDate);
        $startDate = str_replace("-", "/", $startDate);
        $endDate = str_replace("T", " ", $discount->enddate_date_general);
        $endDate = str_replace(".000Z", "", $endDate);
        $endDate = str_replace("-", "/", $endDate);

        $start = Carbon::createFromFormat('Y/m/d H:i:s', $startDate);
        $end = Carbon::createFromFormat('Y/m/d H:i:s', $endDate);

        if(!$now->gte($start) && !$now->lte($end)){
            return false;
        }
        
        if($discount->customer_id_condition != ''){
            if(!isset($customer['id'])){
                return false;
            } else if($customer['id'] != $discount->customer_id_condition) {
                return false;
            }
        }                

        if($discount->generalnumberofuses_num_condition != 0){
            $carts = Db::table('olmo_cart')->where('active', 0)->where('discount', $code)->get();
            if(count($carts) > intVal($discount->generalnumberofuses_num_condition)){
                return false;
            }
        }        

        if($discount->usernumberofuses_num_condition != 0 && $discount->customer_id_condition != ''){
            $carts = Db::table('olmo_cart')->where('customer_id', $customer['customer_id'])->where('active', 0)->where('discount', $code)->get();
            if(count($carts) > intVal($discount->usernumberofuses_num_condition)){
                return false;
            }
        }  

        return $discount;
    }

    public static function getCustomerDiscount($code, $total, $items, $customer){
      
        $discount = self::discountCondition($code, $customer);
        if(!$discount){
            return 0;
        }

        $newTotal = $total;

        if($discount->category_id_condition != ''){
            $newTotal = 0;
            foreach($items as $item){                
                if($discount->category_id_condition == $item['version']['category']['id']){
                    $newTotal = $newTotal + $item['totalprice'];
                }
            }
        }

        if($discount->value_num_general != 0) {
            return $discount->value_num_general;
        } else if($discount->percentage_num_general != 0) {            
            $discount = ($newTotal * $discount->percentage_num_general) / 100;
            return $discount;
        }        

        return 0;
    }    

    public function getCartIdTesting(Request $request)
    {
        // $customer = HelpersCustomer::getCustomer($request);
        $customer = self::getPartialCart($request, $backofficeOrder);
        $cart = self::getCartId($customer, null);
        return $cart;
    }

    public static function getCartProperty($customer, $lang, $orderid_from_backoffice = null){

        $carts = [];
        $defaultLang = HelpersLang::getDefaultLang();
        
        $cart = self::getCartId($customer, $orderid_from_backoffice);
        $items = [];

        if($cart){
            if(is_array($cart->id)){
                $items = Db::table('olmo_cartitem')->whereIn('cart_id', $cart->id)->get();
            } else {
                $items = Db::table('olmo_cartitem')->where('cart_id', $cart->id)->get();
            }
        }
        
        $i = 0;
        $totalprice = 0;        

        foreach ($items as $item) {
            if($item->product_id != ''){
                $product = Db::table('olmo_product')->where('id', $item->product_id)->first();
            } else {
                $product = Db::table('olmo_product')->where('id', $item->item_id)->first();
            }
            $itemid = $product->id;
            // return $product;
            if($item->properties != ''){
                $props = [json_decode($item->properties, true)];
                $filter = '';
                $counter = 0;
                foreach($props as $value){
                    foreach($value as $v){
                        if($counter > 0){
                            $filter .= " AND";
                        }
                        $filter .= " property_props_general like '%$v%'";
                        $counter++;
                    }
                }

                $sql = "SELECT * FROM olmo_productitem WHERE $filter AND enabled_is_general = 'true' AND locale_hidden_general = '$defaultLang' AND postid_hidden_general = '$itemid'";
                $variants = DB::select($sql); 

                if($defaultLang != $lang){
                    $variant = Db::table('olmo_productitem')->where('locale_hidden_general', $lang)->where('parentid_hidden_general', $variants[0]->id)->first();
                    $variants = [$variant];
                }
            }else{
                $variants[0] = $product;
            }
            // TODO: questi sono i dati su cui va in errore per i prodotti senza variante price_num_general, qty_spin_general, quantity, weight_num_general
            $price = $product->price_num_general == floatval(0) ? floatval($variants[0]->price_num_general) : floatval($product->price_num_general);
            $qty = intval($item->item_qty);
            $totalprice = ($price * $qty);

            $carts[$i]['cartitemid'] = $item->id; // item id
            $carts[$i]['version'] = HelpersSerializer::routeResponse($variants[0]); // qui aggiungo la variante
            $carts[$i]['id'] = $item->id; // item id
            $carts[$i]['properties'] = @$carts[$i]['version']['property'];
            $carts[$i]['quantity'] = $qty;
            $carts[$i]['quantity_store'] = $item->properties != '' ? intval($variants[0]->qty_spin_general) : intval($product->qty_spin_general);
            $carts[$i]['type'] = 'product';
            $carts[$i]['weight'] = $product->weight_num_general == floatval(0) ? floatval($variants[0]->weight_num_general) * intval($qty) : floatval($product->weight_num_general) * intval($qty);
            // $carts[$i]['version'] = null;
            $carts[$i]['num_version'] = null;
            $carts[$i]['productname'] = $product->title_txt_content ?? $product->title_txt_general ?? $product->name_txt_general;
            $carts[$i]['productdbname'] = $product->name_txt_general;
            $carts[$i]['available'] = $item->properties != '' ? intval($variants[0]->qty_spin_general) >= intval($item->item_qty) : intval($product->qty_spin_general) >= intval($item->item_qty);
            $carts[$i]['code'] = @$item->properties != '' ? @$variants[0]->sku_txt_general : @$product->sku_txt_general;
            $carts[$i]['singleprice'] = $price;
            $carts[$i]['totalprice'] = $totalprice;

            // foreach($modelStrict as $key=>$value){
            //     $carts[$i][$key] = $e->{$key};
            // }

            // $gQ +=  $item->quantity_num_general;
            $i++;
        }     

        // $object = new stdClass();

        // $discount = self::getCustomerDiscount();
        // $percentage = @$discount['percentage'];
        // $value = @$discount->discount;

        // if($percentage > 0){
        //     $total = $total - (($total/100) * $percentage);
        // }

        // if($value > 0){
        //     $total = $total - $value;
        // }

        $subtotal = 0;
        $weight = 0;

        foreach($carts as $item){
            $subtotal += $item['totalprice'];
        }

        foreach($carts as $item){
            $weight += $item['weight'];
        }

        /**
         * Get the category name
         */
        $category = "";
        if(isset($product->category_id_general)){
            if($product->category_id_general != ""){
                $category = Db::table('olmo_category')->where('id', $product->category_id_general)->first();
            }
        }
        
        $object['category'] = $category;
        $object['weight'] = $weight;
        $object['items'] = $carts;
        $object['subtotal'] = $subtotal;
        $object['discount'] = '';
        $object['shippingcost'] = $cart ? HelpersCart::getShippingCost($cart) : null;
        $object['total'] = $subtotal;

        return $object;
    }


    public function addCart(Request $request)
    {
        $quantity = $request->input('quantity');
        $id = $request->input('id');
        $type = $request->input('type');
        $properties = $request->input('properties');
        $lang = $request->lang;

        if(!$quantity OR $quantity == 'null'){
            $quantity = 1;
        }

        $customer = HelpersCustomer::getCustomer($request);
        $defaultLang = HelpersLang::getDefaultLang();     

        if($defaultLang != $lang){
            $currentitem = DB::table('olmo_product')->where('id', $id)->first();
            $item = DB::table('olmo_product')->where('id', $currentitem->parentid_hidden_general)->first();
            if($item){
                $id = $item->id;
            }

            /** Convert properties */
            $propArray = [];
            foreach($properties as $p){
                $currentproperty = DB::table('olmo_propertyitem')->where('id', $p)->first();
                $dafaultproperty = DB::table('olmo_propertyitem')->where('id', $currentproperty->parentid_hidden_general)->first();
                array_push($propArray, $dafaultproperty->id);       
            }
            $properties = $propArray;
        }   
        

        /**
         * Check if the user has an active cart
         */
        if(isset($customer['id'])){
            $cart = Db::table('olmo_cart')->where('customer_id', $customer['id'])->where('active', '1')->first();
        } else {
            $cart = Db::table('olmo_cart')->where('guest_id', $customer)->where('active', '1')->first();
        }       

        /**
         * Add the item to the db
         */
        if($cart){
            $checkQuantity = HelpersCart::checkQuantity($request, $cart, $type, $quantity);
            if(!$checkQuantity){
                return response(['No availability first step'], 419);
            }

            /**
             * Check if the element is product or item then add to the cartitem
             */
            if($properties){
                $checkProduct = Db::table('olmo_cartitem')->where('cart_id', $cart->id)->where('item_id', $id)->where('properties', json_encode($properties))->first();
                if($checkProduct){
                    $insertProduct = Db::table('olmo_cartitem')->where('cart_id', $cart->id)->where('item_id', $id)->where('properties', json_encode($properties))->increment('item_qty', 1);
                } else {
                    $insertProduct = Db::table('olmo_cartitem')->insertGetId(['lang' => $lang, 'item_id' => $id, 'cart_id' => $cart->id, 'item_qty' => $quantity, 'properties' => json_encode($properties)]);
                }            
            } else {
                $checkProduct = Db::table('olmo_cartitem')->where('cart_id', $cart->id)->where('product_id', $id)->first();
                if($checkProduct){
                    $insertProduct = Db::table('olmo_cartitem')->where('cart_id', $cart->id)->where('product_id', $id)->increment('item_qty', $quantity);
                } else {
                    $insertProduct = Db::table('olmo_cartitem')->insertGetId(['lang' => $lang, 'product_id' => $id, 'cart_id' => $cart->id, 'item_qty' => $quantity]);
                }                
            }

        } else {

            if(isset($customer['id'])){
                $cart = Db::table('olmo_cart')->insertGetId(['customer_id' => $customer['id'], 'active' => '1']);
            } else {
                $cart = Db::table('olmo_cart')->insertGetId(['guest_id' => $customer, 'active' => '1']);    
            }
            $checkQuantity = HelpersCart::checkQuantity($request, $cart, $type);
            if(!$checkQuantity){
                return response(['No availability second step'], 419);
            }
            $checkProduct = Db::table('olmo_cartitem')->where('cart_id', $cart)->where('item_id', $id)->where('properties', json_encode($properties))->first();
            if($checkProduct){
                if($type == 'item'){
                    $insertProduct = Db::table('olmo_cartitem')->where('cart_id', $cart)->where('item_id', $id)->where('properties', json_encode($properties))->increment('item_qty', $quantity);
                } else {
                    $insertProduct = Db::table('olmo_cartitem')->where('cart_id', $cart)->where('product_id', $id)->increment('item_qty', $quantity);
                }
            } else {
                if($type == 'item'){
                    $insertProduct = Db::table('olmo_cartitem')->insertGetId(['lang' => $lang, 'item_id' => $id, 'cart_id' => $cart, 'item_qty' => $quantity, 'properties' => json_encode($properties)]);
                } else {
                    $insertProduct = Db::table('olmo_cartitem')->insertGetId(['lang' => $lang, 'product_id' => $id, 'cart_id' => $cart, 'item_qty' => $quantity]);
                }
            }
                 
        }

        if($insertProduct){
            $getPartialCart = self::getPartialCart($request);
            $getPartialCart['current_item'] = [];
            $getPartialCart['current_product'] = [];
            if(isset($getPartialCart['items'])){
                foreach($getPartialCart['items'] as $item){
                    if($item['id'] == $id){
                        $getPartialCart['current_item'] = $item;
                    }
                }
            } else {
                $getPartialCart['items'] = [];
            }
            if($properties != ""){
                $productmain = Db::table('olmo_product')->where('id', $id)->first();
                $productmain = HelpersSerializer::cleanKeyObj($productmain);
                $getPartialCart['current_product'] = $productmain;
            }
            return $getPartialCart;
        }
        return response(400);

    }

    public function deleteCartItem(Request $request)
    {
        $lang = $request->lang;
        $id = $request->cartitemid;
        $properties = $request->input('properties');

        $customer = HelpersCustomer::getCustomer($request);
        
        if(isset($customer['id'])){
            $carts = Db::table('olmo_cart')->where('customer_id', $customer['id'])->where('active', '1')->get();
            foreach($carts as $cart){
                $response = Db::table('olmo_cartitem')->where('cart_id', $cart->id)->where('id', $id)->delete();
            }
        } else {
            $cart = Db::table('olmo_cart')->where('guest_id', $customer)->where('active', '1')->first();
            $response = Db::table('olmo_cartitem')->where('cart_id', $cart->id)->where('id', $id)->delete();
        }
        

        if($response){
            $getPartialCart = self::getPartialCart($request);

            $getPartialCart['current_item'] = [];
            $getPartialCart['current_product'] = [];
            foreach($getPartialCart['items'] as $item){
                if($item['id'] == $id){
                    $getPartialCart['current_item'] = $item;
                }
            }
            if($properties != ""){
                $productmain = Db::table('olmo_product')->where('id', $id)->first();
                $productmain = HelpersSerializer::cleanKeyObj($productmain);
                $getPartialCart['current_product'] = $productmain;
            }

            return response($getPartialCart, 200);
        }
        return response([], 400);

    }

    public function addQuantity(Request $request)
    {
        return HelpersCart::addQuantity($request);

    }

    public static function getCartForAgentOrder($order_id)
    {               
        $cart = Db::table('olmo_cart')->where('order_id', $order_id)->where('active', 2)->first();
        // Log an informational message

        $items = [];
        if($cart){
            if(is_array($cart->id)){
                $items = Db::table('olmo_cartitem')->whereIn('cart_id', $cart->id)->get();
            } else {
                $items = Db::table('olmo_cartitem')->where('cart_id', $cart->id)->get();
            }

            $qty = [];
            $products = [];
            foreach($items as $item){
                $product = Db::table('olmo_product')->where('id', $item->item_id)->first();
                $singleitame = null;
                if($item->properties != "") {
                    $singleitame = Db::table('olmo_productitem')->where('id', $item->item_id)->first();
                    $singleitame = HelpersSerializer::cleanKeyObj($singleitame);
                }
                $product = HelpersSerializer::cleanKeyObj($product);
                $product['cartitemid'] = $item->id;
                $product['type'] = '';
                $product['version'] = isset($singleitame['name']) ? $singleitame['name'] : $product['name'];
                $product['quantity'] = $item->item_qty;
                array_push($qty, (int)$item->item_qty);
                array_push($products, $product);
            }        

            return json_encode($cart['items']);;            
        }
        return json_encode([]);
    }

    public static function getCartObjects($cart_id){
        $all_items = Db::table('olmo_cartitem')->where('cart_id', $cart_id)->orderBy('id', 'desc')->get();
        $response = [];

        foreach($all_items as $key=>$item){

            if($item->product_id == 0){ // item non aggiunto
                $response[$key] = [
                    'product' => '',
                    'cartitem_id' => $item->id,
                    'variant' => '',
                    'qty' => ''
                ];   
            } else {
                $product = Db::table('olmo_product')->where('id', $item->product_id)->first();
                $response[$key] = [
                    'product' => $product->name_txt_general,
                    'cartitem_id' => $item->id, 
                    'qty' => $item->item_qty
                ];

                $response[$key]['variant'] = '';
                if($item->variant_id != 0){
                    $props_string = implode(", ", json_decode($item->properties));
                    $variant = Db::table('olmo_productitem')->where('postid_hidden_general', $product->id)->where('property_props_general', $props_string)->first();
                    $response[$key]['variant'] = $variant->name_txt_general;
                }
            }
        }
        
        return $response;
    }

    public static function UpdateProductAgentCart($request){ 
        $cartitem_id = $request->input('cartitemid');
        $product_id = $request->input('productid');
        $variant_id = $request->input('variantid');
        $props = $request->input('properties');
        $qty = $request->input('qty');

        $fields = [
            'item_id' => $product_id,
            'product_id' => $product_id,
            'variant_id' => $variant_id,
            'item_qty' => $qty,
            'properties' => $props
        ];

        if( Db::table('olmo_cartitem')->where('id', $cartitem_id)->limit(1)->update($fields) ){
            $cartitem = Db::table('olmo_cartitem')->where('id', $cartitem_id)->first();
            $cart_obj = self::getCartObjects($cartitem->cart_id);
            return response(['cart_id' => $cartitem->cart_id, 'cart' => $cart_obj], 200);
        }else{
            return response("Can't update", 400);
        }  
    }

    public static function getCartByRoute($request){
        $order_id = $request->input('orderid', false);
        if (!$order_id) {
            return response('Order id not found!', 400);
        }
        $customer_email = $request->input('customer', false);
        if (!$customer_email) {
            return response('Customer email not found!', 400);
        }

        $cart = Db::table('olmo_cart')->where('order_id', $order_id)->where('backoffice', 1)
        ->whereRaw("customer_id = (select id from olmo_customer where email_email_general = ?)", [$customer_email])
        ->first(); 

        $order = Db::table('olmo_order')->where('id', $order_id)->where('orderitems_json_items', "!=", '[]')->first();
        if($cart){
            return response(['cart' => self::getCartObjects($cart->id), "cart_id" => $cart->id, "orderplaced" => $order ? 1:0], 200);
        }else {
            return response('Not found', 400);
        }
        
    }  

    public static function DeleteProductFromAgentCart($request){
        $cartitem_id = $request->input('cartitemid');

        if($cartitem = Db::table('olmo_cartitem')->where('id', $cartitem_id)->first()){
            Db::table('olmo_cartitem')->where('id', $cartitem_id)->delete();
            return response(['cart' => self::getCartObjects($cartitem->cart_id)], 200);
        }
        return response("Not permitted", 400);
    }

    public static function createCartItem($request){
        $order_id = $request->input('orderid');
        $customer_email = $request->input('customer');
        $lang = $request->lang; 
        $order = [];
        $cart = [];
        $cart_id = '';

        $cart = Db::table('olmo_cart')->where('order_id', $order_id)->where('backoffice', 1)->first();

        if($cart){
            $cart_id = $cart->id;
        } else {
            $customer = Db::table('olmo_customer')->where('email_email_general', $customer_email)->first();
            $cart_id = Db::table('olmo_cart')->insertGetId([
                'order_id' => $order_id,
                'customer_id' => $customer->id,
                'active' => 1,
                'backoffice' => 1
            ]);
            $cart = Db::table('olmo_cart')->where('id', $cart_id)->first();
        }

        $cartitem_id = false;
        if( $cartitem = Db::table('olmo_cartitem')->where('cart_id', $cart_id)->where('product_id', 0)->first() ){
            $cartitem_id = $cartitem->id;
        } else {
            $fields = [
                'cart_id' => $cart_id,
                'product_id' => 0,
                'variant_id' => 0,
                'item_id' => 0,
                'item_qty' => 0,
                'properties' => '',
                'lang' => $lang
            ];
            $cartitem_id = Db::table('olmo_cartitem')->insertGetId($fields);
        }
        
        if($cartitem_id){
            $cart_obj = self::getCartObjects($cart_id);
            return response(['cart_id' => $cart_id, 'cart' => $cart_obj], 200);
        }else{
            return response("Can't create", 500);
        }
        
    }

}
