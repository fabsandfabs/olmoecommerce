<?php

namespace Olmo\Ecommerce\Containers\Ecommerce\Order\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use Olmo\Ecommerce\App\Http\Controller\EcommerceController;
use Olmo\Core\App\Helpers\HelpersCustomer;
use Olmo\Core\App\Helpers\HelpersSerializer;
use Olmo\Core\App\Helpers\HelpersUser;
use Olmo\Core\App\Helpers\HelpersDBRelation;
use Olmo\Core\App\Helpers\HelpersExtra;

use Olmo\Ecommerce\App\Helpers\HelpersCart;
use Olmo\Ecommerce\App\Helpers\HelpersPayment;
use Olmo\Ecommerce\Containers\Ecommerce\Cart\Controllers\Controller as Cart;
use Olmo\Ecommerce\App\Helpers\HelpersEmail;
use Olmo\Ecommerce\App\Helpers\HelpersOrder;

use Illuminate\Support\Facades\Schema;


class Controller extends EcommerceController
{   

    /**
     * 
     * Vulnerabilitá: 
     * - se sono presenti piú ordini non chiusi, vengono presi tutti i dati del carrello
     * - se é sbagliato l'order id, vengono comunque presi i dati dei carrelli attivi di un preciso customer
     * 
     * Soluzione:
     * - Aggiungere un parametro che identifica l'esatto
     * 
     */
    public static function orderCompleted(Request $request){
        $response = ['ordercomplete' => 0, "emailsent" => 0];
        $order_id = HelpersExtra::checkRequestInput($request, 'orderid'); // useless, trova un altro modo

        $cart = Cart::getFullCart($request, true);
        // return response(json_encode($cart), 200);
        $order['totalitems_read_general'] = $cart['items_quantity'];
        $order['subtotal_read_general'] = $cart['subtotal'];
        $order['discountvalue_read_general'] = 0.00;
        $order['total_read_general'] = $cart['total'];
        $order['shippingcost_read_general'] = $cart['shippingcost'];
        $order['orderstatus_select_general'] = 'completed';
        
        Db::table('olmo_order')->where('id', $order_id)->update(array_merge(['orderitems_json_items' => json_encode($cart['items'], true)], $order));
        $response['ordercomplete'] = 1;
 
        // abbiamo deciso di non processare l'ordine attraverso il classico updatePost/Save
        // ma facciamo una rotta apposta.
        // quindi a frontend  andiamo a create un bottone che si attiva quando ci sono tutte le condizioni necessarie per far generare un ordine
        // parallelamente aggiungere una var nell'env che se true manda anche l'email, se invece non è tru bisogna creare un bottone che manda l'email al click
        
        if($request->hasHeader('x-agent') && env('AGENT_ORDER_SEND_EMAIL', false)){
            $email = $request->header('x-agent');

            $customer = HelpersSerializer::cleanKeyObj(HelpersCustomer::getCustomerFromEmail($email), true);
            $theOrder = Db::table('olmo_order')->where('id', $order_id)->first();
            $theOrderObject = HelpersSerializer::routeResponse($theOrder); 
    
            HelpersEmail::sendEmail($customer, $theOrderObject, $customer['customerlang'], '28', false);
            Db::table('olmo_order')->where('id', $order_id)->update(['emailsent_hidden_general' => 1]);
            $response['emailsent'] = 1;
        }

        return response($response, 200);
    }

    
    public function placeOrder(Request $request){
        $lang = $request->lang;
        $customer = HelpersCustomer::getCustomer($request);
        if (!$customer) {
            return response(['User Session not found'], 403);
        }

        if(is_array($customer)){
            $customer = HelpersSerializer::cleanKeyObj($customer);
        } else {
            $guest = Db::table('olmo_customer')->where('xguest_read_general', $customer)->first();
            $guest = HelpersSerializer::cleanKeyObj($guest);
        }           

        $cart = Cart::getFullCart($request);        

        /**
         * CHECK QUANTITY AGAIN
         */
        $check = true;
        if(!$check){
            return response(['Sorry no quantity available for the following product'], 400);
        }

        $order = [];
        $name_and_code = time().rand(1000,9999);
        $order['code_read_general'] = $name_and_code;
        $order['createdonutc_read_general'] = date("Y-m-d H:i:s", time() - date("Z"));
        $order['lastmod_read_information'] = date("Y-m-d H:i:s");        
        $order['paidonutc_read_general'] = date("Y-m-d H:i:s");        
        $order['orderstatus_select_general'] = 'pending';
        $order['shippingmethod_txt_general'] = $cart['selectedshippingmethod']['code'];
        $order['paymentmethod_txt_general'] = $cart['selectedpaymentmethod']['code'];
        // $order['paidonutc_read_general'] = '';
        $order['totalitems_read_general'] = $cart['items_quantity'];
        $order['subtotal_read_general'] = $cart['subtotal'];
        $order['discountvalue_read_general'] = 0.00;
        $order['total_read_general'] = $cart['total'];
        $order['currency_read_general'] = 'EUR';
        $order['shippingcost_read_general'] = $cart['shippingcost'];
        // $order['discountcode_hidden_general'] = '';

        $order['name_txt_general'] = $name_and_code;
        $order['trash_hidden_general'] = '';
        $order['createdby_read_information'] = is_array($customer) ? $customer['email'] : $guest['xguest'];
        $order['lastmodby_read_information'] = is_array($customer) ? $customer['email'] : $guest['xguest'];
        $order['customerid_hidden_customer'] = is_array($customer) ? $customer['id'] : $guest['id'];
        $order['customer_read_general'] = is_array($customer) ? $customer['email'] : $guest['email'];

        /**
         * Shipping
         */
        $shippingId = $cart['addresses']['shipping'];
        $shipping = Db::table('olmo_customeritem')->where('id', $shippingId)->first();
        $shipping = HelpersSerializer::cleanKeyObj($shipping);        
        $order['courierlink_txt_shipping'] = '';
        $order['tracknumber_txt_shipping'] = '';
        $order['shippingname_read_shipping'] = $shipping['name'];
        $order['shippingsurname_read_shipping'] = $shipping['surname'];
        $order['shippingphone_read_shipping'] = $shipping['phone'];
        $order['shippingcompany_read_shipping'] = $shipping['company'];
        $order['shippingaddress_read_shipping'] = $shipping['address'];
        $order['shippingaddressnumber_read_shipping'] = $shipping['number'];
        $order['shippingcity_read_shipping'] = $shipping['city'];
        // $order['shippingregion_read_shipping'] = $shipping['region'];
        $order['shippingstate_read_shipping'] = '';
        $order['shippingcountry_read_shipping'] = $shipping['country'];
        $order['shippingZIP_read_shipping'] = $shipping['zip'];        
        $order['shippingprovincia_read_shipping'] = $shipping['provincia'];    

        /**
         * Billing
         */
        $billingId = $cart['addresses']['billing'];
        if($billingId == 'samehashipping'){
            $billing = Db::table('olmo_customeritem')->where('id', $shipping)->first();
        } else {
            $billing = Db::table('olmo_customeritem')->where('id', $billingId)->first();
        }
        $billing = HelpersSerializer::cleanKeyObj($billing);
        $order['billingname_read_billing'] = $billing['name'];
        $order['billingsurname_read_billing'] = $billing['surname'];
        $order['billingphone_read_billing'] = $billing['phone'];
        $order['billingcompany_read_billing'] = $billing['company'];
        $order['billingaddress_read_billing'] = $billing['address'];
        $order['billingaddressnumber_read_billing'] = $billing['number'];
        $order['billingcity_read_billing'] = $billing['city'];
        $order['billingregion_read_billing'] = $billing['region'];
        $order['billingstate_read_billing'] = '';
        $order['billingcountry_read_billing'] = $billing['country'];
        $order['billingZIP_read_billing'] = $billing['zip'];
        $order['billingprovincia_read_billing'] = $billing['provincia'];    
    
        /* Order items */
        $order['orderitems_json_items'] = json_encode($cart['items']);
        
        /* Paypal id will be filled later */
        $order['paypalid_txt_general'] = '';

        $id = Db::table('olmo_order')->insertGetId($order);
        $theOrder = Db::table('olmo_order')->where('id', $id)->first();
        $theOrderObject = HelpersSerializer::routeResponse($theOrder); 
        $theOrder = json_decode(json_encode($theOrderObject), true);

        /**
         * Detect payment method
         */  
        $paymentMethod = $order['paymentmethod_txt_general'];

        /**
         * If payment method are those send the email
         */
        if($paymentMethod == 'email' OR $paymentMethod == 'banktransfer'){
            $customer = is_array($customer) ? $customer : $guest;
            /** value 28 stands for Form Type Orderplaced  */
            $theOrder['send'] = HelpersEmail::sendEmail($customer, $theOrderObject, $lang, '28', false);
            $theOrder['paymentmethod'] = $paymentMethod;
            return response($theOrder, 200);

        }

        /**
         * If payment method is paypal process the payment 
         */        
        if($paymentMethod == 'paypal'){
            $customer = is_array($customer) ? $customer : $guest;
            $paypal = HelpersPayment::createOrderPaypal($order, $order['currency_read_general'], $lang);
            $theOrder['send'] = HelpersEmail::sendEmail($customer, $theOrderObject, $lang, '28', false);
            $theOrder['paypal'] = $paypal->json();
            $theOrder['paymentmethod'] = $paymentMethod;
            return $theOrder;
        }

        /**
         * If payment method is credit card with paypal to process the payment 
         */        
        if($paymentMethod == 'creditcard' AND env('CREDITCARD_METHOD') == 'paypal'){
            $customer = is_array($customer) ? $customer : $guest;
            $paypal = HelpersPayment::createOrderPaypal($order, $order['currency_read_general'], $lang);
            $theOrder['send'] = HelpersEmail::sendEmail($customer, $theOrderObject, $lang, '28', false);
            $theOrder['paypal'] = $paypal->json();
            $theOrder['paymentmethod'] = $paymentMethod;
            return $theOrder;
        }
        
        return response($theOrder, 200);
    }

    public static function orderConfirmEmail($request){
        // TODO: fai un metodo unico per recuperare e checkare gli input e header...
        $email = $request->header('x-agent', false);
        $order_id = $request->input('orderid', false);
        $cart_id = $request->input('cartid', false);
        $cart_obj = Cart::getCartObjects($cart_id);

        if($email != false){
            $customer = HelpersSerializer::cleanKeyObj(HelpersCustomer::getCustomerFromEmail($email), true);
            // $customer = json_decode(json_encode(), true);
            $theOrder = Db::table('olmo_order')->where('id', $order_id)->first();
            $theOrderObject = HelpersSerializer::routeResponse($theOrder); 
    
            HelpersEmail::sendEmail($customer, $theOrderObject, $customer['customerlang'], '28', false);
            
            return response(['cart_id' => $cart_id, 'cart' => $cart_obj, "sent" => 1], 200);
        }
        return response(['cart_id' => $cart_id, 'cart' => $cart_obj, "sent" => 0], 200);
    }

    public function orderEmailPayment($order_id, $order){
        $theOrder = Db::table('olmo_order')->where('id', $order_id)->first();
        $theOrderObject = HelpersSerializer::routeResponse($theOrder); 
        $theOrder = json_decode(json_encode($theOrderObject), true);

        /**
         * Detect payment method
         */  
        $paymentMethod = $order['paymentmethod_txt_general'];

        /**
         * If payment method are those send the email
         */
        if($paymentMethod == 'email' OR $paymentMethod == 'banktransfer'){

            /** value 28 stands for Form Type Orderplaced  */
            $theOrder['send'] = HelpersEmail::sendEmail($customer, $theOrderObject, $lang, '28', false);
            $theOrder['paymentmethod'] = $paymentMethod;
            return response($theOrder, 200);

        }

        /**
         * If payment method is paypal process the payment 
         */        
        if($paymentMethod == 'paypal'){
            $paypal = HelpersPayment::createOrderPaypal($order, $order['currency_read_general'], $lang);
            $theOrder['send'] = HelpersEmail::sendEmail($customer, $theOrderObject, $lang, '28', false);
            $theOrder['paypal'] = $paypal->json();
            $theOrder['paymentmethod'] = $paymentMethod;
            return $theOrder;
        }

        /**
         * If payment method is credit card with paypal to process the payment 
         */        
        if($paymentMethod == 'creditcard' AND env('CREDITCARD_METHOD') == 'paypal'){
            $paypal = HelpersPayment::createOrderPaypal($order, $order['currency_read_general'], $lang);
            $theOrder['send'] = HelpersEmail::sendEmail($customer, $theOrderObject, $lang, '28', false);
            $theOrder['paypal'] = $paypal->json();
            $theOrder['paymentmethod'] = $paymentMethod;
            return $theOrder;
        }
    }

    public function getListOrders(Request $request)
    {
        $customer = HelpersCustomer::getCustomer($request);

        if (!$customer) {
            return response(['User Session not found'], 403);
        } 

        $orders = Db::table('olmo_order')->where('customerid_hidden_customer', $customer['id'])->get();
        $orders = HelpersSerializer::cleanKeyArray($orders);

        return $orders;
    }

    public function getSingleOrderByCode(Request $request)
    {
        $code = $request->code;
        $customer = HelpersCustomer::getCustomer($request);
        if(!is_array($customer)){
            $customerid = $customer;
            $customer = Db::table('olmo_customer')->where('xguest_read_general', $customer)->first();
        }        
        $order = false;
        
        if (!$customer) {
            return response(['User Session not found'], 403);
        }        
        
        if($code){
            if(!is_array($customer)){
                $order = Db::table('olmo_order')->where('code_read_general', $code)->where('createdby_read_information', $customerid)->first();
            }else{
                $order = Db::table('olmo_order')->where('code_read_general', $code)->first();
            }
        }
        
        if($order){
            $theOrderObject = HelpersSerializer::cleanKeyObj($order); 
            // $theOrderObject = json_decode($theOrderObject, true);

            /**
             * Add the real name of a country insted of the code
             */
            if(isset($theOrderObject['billingcountry'])){
                if($theOrderObject['billingcountry'] != ''){
                    $getCountry = Db::table('olmo_country')->where('name_txt_general', $theOrderObject['billingcountry'])->first();
                    $theOrderObject['billingcountry'] = $getCountry->title_txt_general;
                }
            }
            if(isset($theOrderObject['shippingcountry'])){
                if($theOrderObject['shippingcountry'] != ''){
                    $getCountry = Db::table('olmo_country')->where('name_txt_general', $theOrderObject['shippingcountry'])->first();
                    $theOrderObject['shippingcountry'] = $getCountry->title_txt_general;
                }
            }
             
            return response($theOrderObject, 200);
        }
        
        return response(400);
        
    }    

    public function completeOrder(Request $request)
    {

        $orderid = $request->input('orderid');
        $token = $request->input('token');
        $lang = $request->lang;             

        $customer = HelpersCustomer::getCustomer($request);
        if (!$customer) {
            return response(['User Session not found'], 403);
        }

        if(!is_array($customer)){
            $customer = Db::table('olmo_customer')->where('xguest_read_general', $customer)->first();
        }  
        

        $order = Db::table('olmo_order')->where('code_read_general', $orderid)->first();    
        $theOrderObject = HelpersSerializer::cleanKeyObj($order); 
        $customer = HelpersSerializer::cleanKeyObj($customer);        

        if(($order->paymentmethod_txt_general == 'paypal' OR $order->paymentmethod_txt_general == 'creditcard') AND $order->orderstatus_select_general == 'pending'){
            $capture = HelpersPayment::capturePaymentPaypal($token);
            if($capture->status() == 201 OR $capture->status() == 200){
                Db::table('olmo_order')->where('code_read_general', $orderid)->update([
                    'orderstatus_select_general' => 'paid',
                    'paypalid_txt_general' => $token
                ]);

                $send = HelpersEmail::sendEmail($customer, $theOrderObject, $lang, '5');

                /**
                 * Reset cart and subtract quantity
                 */
                HelpersOrder::resetCartSubtractQty($customer);

                return response([$capture, $send], $capture->status());
            }
            
            return response($capture, $capture->status());
            
        } else if($order->paymentmethod_txt_general == 'banktransfer' OR $order->paymentmethod_txt_general == 'email'){
            /**
             * Reset cart and subtract quantity
             */
            $cart = HelpersOrder::resetCartSubtractQty($customer);

            return response($cart, 200);
        }

        return response(json_decode(json_encode($order, true), true), 400);
        
    }   

    public static function createNewOrder(Request $request)
    {
        $agent = HelpersUser::getUser();
        $agent = HelpersSerializer::cleanKeyObj($agent);

        $order = [];
        $order = array_merge($order, self::setOlmoInformation());
        $order = array_merge($order, self::setOrderInfo($agent, '', true));

        $order['orderstatus_select_general'] = 'pending';
        $order['shippingmethod_txt_general'] = '';
        $order['paymentmethod_txt_general'] = '';
        // $order['paidonutc_read_general'] = '';
        $order['totalitems_read_general'] = 0;
        $order['subtotal_read_general'] = 0.00;
        $order['discountvalue_read_general'] = 0.00;
        $order['total_read_general'] = 0.00;
        $order['currency_read_general'] = 'EUR';
        $order['shippingcost_read_general'] = 0.00;
        // important for the division between front and back orders
        $order['backoffice_hidden_general'] = 1;
        // $order['discountcode_hidden_general'] = '';
        
        
        /**
         * Billing
         */
        // $billingId = $cart['addresses']['billing'];
        $order = array_merge($order, self::setBillingAddresses('', true));

        /**
         * Shipping
         */
        // $shippingId = $cart['addresses']['shipping'];
        $order = array_merge($order, self::setShippingAddresses('', true));
    
        /* Order items */
        $order['orderitems_json_items'] = json_encode([]);
        
        /* Paypal id will be filled later */
        $order['paypalid_txt_general'] = '';
        $order['enabled_is_general'] = '';
        $order['preview_hidden_general'] = '';
        $order['position_ord_general'] = 0;

        $id = Db::table('olmo_order')->insertGetId($order);
        $theOrder = Db::table('olmo_order')->where('id', $id)->first();
        $theOrderObject = HelpersSerializer::routeResponse($theOrder); 
        $theOrder = json_decode(json_encode($theOrderObject), true);

        // $paymentMethod = $order['paymentmethod_txt_general'];
        
        return response($theOrder, 200);

    } 
    
    public static function setOlmoInformation(){
        $name_and_code = time().rand(1000,9999);
        $order['code_read_general'] = $name_and_code;
        $order['name_txt_general'] = $name_and_code;
        $order['createdonutc_read_general'] = date("Y-m-d H:i:s", time() - date("Z"));
        $order['lastmod_read_information'] = date("Y-m-d H:i:s");        
        $order['paidonutc_read_general'] = date("Y-m-d H:i:s");     
        return $order;
    }

    public static function setBillingAddresses($billingId, $empty = false){
        $order = [];
        if($empty){
            $order['billingname_read_billing'] = '';
            $order['billingsurname_read_billing'] = '';
            $order['billingphone_read_billing'] = '';
            $order['billingcompany_read_billing'] = '';
            $order['billingaddress_read_billing'] = '';
            $order['billingaddressnumber_read_billing'] = '';
            $order['billingcity_read_billing'] = '';
            $order['billingregion_read_billing'] = '';
            $order['billingstate_read_billing'] = '';
            $order['billingcountry_read_billing'] = '';
            $order['billingZIP_read_billing'] = '';
            $order['billingprovincia_read_billing'] = '';
        }else{
            $billing = Db::table('olmo_customeritem')->where('id', $billingId)->first();
            $billing = HelpersSerializer::cleanKeyObj($billing);
            $order['billingname_read_billing'] = $billing['name'];
            $order['billingsurname_read_billing'] = $billing['surname'];
            $order['billingphone_read_billing'] = $billing['phone'];
            $order['billingcompany_read_billing'] = $billing['company'];
            $order['billingaddress_read_billing'] = $billing['address'];
            $order['billingaddressnumber_read_billing'] = $billing['number'];
            $order['billingcity_read_billing'] = $billing['city'];
            $order['billingregion_read_billing'] = $billing['region'];
            $order['billingstate_read_billing'] = '';
            $order['billingcountry_read_billing'] = $billing['country'];
            $order['billingZIP_read_billing'] = $billing['zip'];
            $order['billingprovincia_read_billing'] = $billing['provincia'];
        }

        return $order;
    }

    public static function setShippingAddresses($shippingId, $empty = false){
        $order = [];
        if($empty){
            $order['courierlink_txt_shipping'] = '';
            $order['tracknumber_txt_shipping'] = '';
            $order['shippingname_read_shipping'] = '';
            $order['shippingsurname_read_shipping'] = '';
            $order['shippingphone_read_shipping'] = '';
            $order['shippingcompany_read_shipping'] = '';
            $order['shippingaddress_read_shipping'] = '';
            $order['shippingaddressnumber_read_shipping'] = '';
            $order['shippingcity_read_shipping'] = '';
            // $order['shippingregion_read_shipping'] = $shipping['region'];
            $order['shippingstate_read_shipping'] = '';
            $order['shippingcountry_read_shipping'] = '';
            $order['shippingZIP_read_shipping'] = '';        
            $order['shippingprovincia_read_shipping'] = '';  
        }else{
            $shipping = Db::table('olmo_customeritem')->where('id', $shippingId)->first();
            $shipping = HelpersSerializer::cleanKeyObj($shipping);        
            $order['courierlink_txt_shipping'] = '';
            $order['tracknumber_txt_shipping'] = '';
            $order['shippingname_read_shipping'] = $shipping['name'];
            $order['shippingsurname_read_shipping'] = $shipping['surname'];
            $order['shippingphone_read_shipping'] = $shipping['phone'];
            $order['shippingcompany_read_shipping'] = $shipping['company'];
            $order['shippingaddress_read_shipping'] = $shipping['address'];
            $order['shippingaddressnumber_read_shipping'] = $shipping['number'];
            $order['shippingcity_read_shipping'] = $shipping['city'];
            // $order['shippingregion_read_shipping'] = $shipping['region'];
            $order['shippingstate_read_shipping'] = '';
            $order['shippingcountry_read_shipping'] = $shipping['country'];
            $order['shippingZIP_read_shipping'] = $shipping['zip'];        
            $order['shippingprovincia_read_shipping'] = $shipping['provincia'];  
        }

        return $order;
    }

    public static function getShippingMethod($order_id){
        $code = Db::table('olmo_order')->select('shippingmethod_txt_general as code')->where('id', $order_id)->first();
        $methods = Db::table('olmo_shippingmethod')->where('code_txt_general', $code->code)->first();
        if($code && $methods){
            return ['value' => $methods->id, 'key' => $code->code];
        }else{
            return ['value' => '', 'key' => ''];
        }
    }

    public static function getAddressesValueId($orderId, $type = 'shipping'){
        $types = ['shipping' => 1, 'billing' => 0];
        $type = $types[$type];
        $fields_order_customer = self::mapFields();
        $count = count($fields_order_customer);
        $i = 1;
        $where = '';
        $values = Db::table('olmo_order')->where('id', $orderId)->first();

        foreach($fields_order_customer as $key => $field){
            $field = $field[$type];
            $where = $where."$key = '".$values->$field."'"; 
            $shipping_fields[] = $field;
            if($i != $count){
                $where = $where." AND ";
                $i++;
            }
        }
        
        $method = Db::table('olmo_customeritem')->select('id', 'address_txt_address as key')->whereRaw($where)->first();
        if($method){
            return ['value' => $method->id, 'key' => $method->key];
        }else{
            return ['value' => '', 'key' => ''];
        }

    }

    public static function getPaymentValueId($orderId){
        $payment = Db::table('olmo_order')->select('paymentmethod_txt_general as code')->where('id', $orderId)->first();
        
        $method = Db::table('olmo_paymentmethod')->where('code_txt_general', $payment->code)->first();
        if($method){
            return ['value' => $method->id, 'key' => $method->code_txt_general];
        }else{
            return ['value' => '', 'key' => ''];
        }

    }

    public static function mapFields(){
        return [
            'name_txt_general' => ['billingname_read_billing','shippingname_read_shipping'],
            'surname_txt_address' => ['billingsurname_read_billing','shippingsurname_read_shipping'],
            'company_txt_address' => ['billingcompany_read_billing','shippingcompany_read_shipping'],
            'phone_txt_address' => ['billingphone_read_billing','shippingphone_read_shipping'],
            'address_txt_address' => ['billingaddress_read_billing','shippingaddress_read_shipping'],
            'city_txt_address' => ['billingcity_read_billing','shippingcity_read_shipping'],
            // billingaddressnumber_read_billing / shippingaddressnumber_read_shipping
            // billingstate_read_billing / shippingstate_read_shipping
            'provincia_txt_address' => ['billingprovincia_read_billing','shippingprovincia_read_shipping'],
            'zip_txt_address' => ['billingZIP_read_billing','shippingZIP_read_shipping'],
            // 'region_txt_address' => ['billinggregion_read_billing','shippingregion_read_shipping'],
            'country_txt_address' => ['billingcountry_read_billing','shippingcountry_read_shipping'],
        ];
    }

    public static function allProduct($request){
        $product_with_variant = Db::select("SELECT * FROM olmo_product WHERE enabled_is_general = 'true' AND items_productitems_general != ''");
        $product_without_variant = Db::select("SELECT * FROM olmo_product WHERE enabled_is_general = 'true' AND items_productitems_general = ''");
        $list = HelpersDBRelation::dropdownListValuePair($product_with_variant, true);
        foreach($list as &$product){
            $product['hasvariant'] = true;
        }
        $list2 = HelpersDBRelation::dropdownListValuePair($product_without_variant, true);
        foreach($list2 as &$product){
            $product['hasvariant'] = false;
        }
        return array_merge($list, $list2);
    }

    public static function allVariant($id){
        $variant_list = Db::table('olmo_productitem')->where('enabled_is_general', 'true')->where("postid_hidden_general", $id)->get();
        $list = HelpersDBRelation::dropdownListValuePair($variant_list, true);

        $list[0]['properties'] = '';
        foreach($variant_list as $key => $variant){
            $key = $key + 1;
            $props_array = json_encode(explode(',', str_replace(' ', '', $variant->property_props_general) ), true);

            $list[$key]['properties'] = $props_array;
        }
        return $list;
    }

    public static function setOrderInfo($agent, $customer, $empty = false){

        if($empty){
            $order['trash_hidden_general'] = '';
            $order['createdby_read_information'] = $agent['email'];
            $order['lastmodby_read_information'] = $agent['email'];
            $order['customerid_hidden_customer'] = '';
            $order['customer_read_general'] = ''; 
        }else{
            $order['trash_hidden_general'] = '';
            $order['createdby_read_information'] = $agent['email'];
            $order['lastmodby_read_information'] = $agent['email'];
            $order['customerid_hidden_customer'] = $customer['id'];
            $order['customer_read_general'] = $customer['email']; 
        }      
        
        return $order;
    }
    
}
