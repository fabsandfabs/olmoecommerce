<?php

use Olmo\Ecommerce\Containers\Ecommerce\Order\Controllers\Controller;
use Illuminate\Support\Facades\Route;

/** Order */
Route::post('/{lang}/order', [Controller::class, 'placeOrder']);

Route::get('/{lang}/order', [Controller::class, 'getListOrders']);

Route::get('/{lang}/order/{id}', [Controller::class, 'getSingleOrder']);

Route::patch('/{lang}/order/{id}', [Controller::class, 'chnageStatusOrder']);

Route::get('/{lang}/orderbycode/{code}', [Controller::class, 'getSingleOrderByCode']);

Route::post('/{lang}/order/completed', [Controller::class, 'completeOrder']);

